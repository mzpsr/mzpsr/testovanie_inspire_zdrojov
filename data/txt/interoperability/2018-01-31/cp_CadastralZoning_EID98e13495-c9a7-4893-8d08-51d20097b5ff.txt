31.01.2018 01:12:27 - Preparing Test Run interoperability_null_GeodeticandCartographicInstituteBratislava_cp:CadastralZoning (initiated Wed Jan 31 01:12:27 CET 2018)
31.01.2018 01:12:27 - Resolving Executable Test Suite dependencies
31.01.2018 01:12:27 - Preparing 10 Test Task:
31.01.2018 01:12:27 -  TestTask 1 (c5251ddd-4059-42f5-9c11-43d1c21b7911)
31.01.2018 01:12:27 -  will perform tests on Test Object 'service.xml' by using Executable Test Suite 'LAZY.545f9e49-009b-4114-9333-7ca26413b5d4'
31.01.2018 01:12:27 -  with parameters: 
31.01.2018 01:12:27 - etf.testcases = *
31.01.2018 01:12:27 -  TestTask 2 (0860e2fd-d3e0-4ff3-9309-463e6c31abc4)
31.01.2018 01:12:27 -  will perform tests on Test Object 'service.xml' by using Executable Test Suite 'LAZY.09820daf-62b2-4fa3-a95f-56a0d2b7c4d8'
31.01.2018 01:12:27 -  with parameters: 
31.01.2018 01:12:27 - etf.testcases = *
31.01.2018 01:12:27 -  TestTask 3 (963be512-74a3-464f-a892-6d16f30defde)
31.01.2018 01:12:27 -  will perform tests on Test Object 'service.xml' by using Executable Test Suite 'LAZY.18b742d0-15eb-421f-bbec-7c8c5cf7ee1a'
31.01.2018 01:12:27 -  with parameters: 
31.01.2018 01:12:27 - etf.testcases = *
31.01.2018 01:12:27 -  TestTask 4 (204c0625-0a92-48f4-83c5-31e98f5b4a14)
31.01.2018 01:12:27 -  will perform tests on Test Object 'service.xml' by using Executable Test Suite 'Conformance class: Application Schema, Cadastral parcel (EID: 1f9bc92a-5879-4e9b-bcbe-1d2d0cab0aab, V: 0.2.2 )'
31.01.2018 01:12:27 -  with parameters: 
31.01.2018 01:12:27 - etf.testcases = *
31.01.2018 01:12:27 -  TestTask 5 (566a0330-aed6-4e90-ae0a-992068590af8)
31.01.2018 01:12:27 -  will perform tests on Test Object 'service.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
31.01.2018 01:12:27 -  with parameters: 
31.01.2018 01:12:27 - etf.testcases = *
31.01.2018 01:12:27 -  TestTask 6 (2f8df74c-5a63-4464-99a4-4964245117b7)
31.01.2018 01:12:27 -  will perform tests on Test Object 'service.xml' by using Executable Test Suite 'Conformance class: Data consistency, Cadastral Parcels (EID: 92032cdb-db88-42aa-96c0-70a1af9e68b1, V: 0.2.0 )'
31.01.2018 01:12:27 -  with parameters: 
31.01.2018 01:12:27 - etf.testcases = *
31.01.2018 01:12:27 -  TestTask 7 (323a2e12-aa04-4926-99b5-af710b4913f3)
31.01.2018 01:12:27 -  will perform tests on Test Object 'service.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
31.01.2018 01:12:27 -  with parameters: 
31.01.2018 01:12:27 - etf.testcases = *
31.01.2018 01:12:27 -  TestTask 8 (6c1086d3-5d1f-41c7-bfc7-07ff61c7f9c9)
31.01.2018 01:12:27 -  will perform tests on Test Object 'service.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Cadastral Parcels (EID: c4fbae00-3070-49fa-b803-24c66c31ac70, V: 0.2.1 )'
31.01.2018 01:12:27 -  with parameters: 
31.01.2018 01:12:27 - etf.testcases = *
31.01.2018 01:12:27 -  TestTask 9 (4c052b1e-903b-4c83-bd54-0d0bf60cd7dd)
31.01.2018 01:12:27 -  will perform tests on Test Object 'service.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
31.01.2018 01:12:27 -  with parameters: 
31.01.2018 01:12:27 - etf.testcases = *
31.01.2018 01:12:27 -  TestTask 10 (7c80d874-3b53-45c1-ae63-7910183d375c)
31.01.2018 01:12:27 -  will perform tests on Test Object 'service.xml' by using Executable Test Suite 'Conformance class: Reference Systems, Cadastral Parcels (EID: dbcc48ae-6871-4444-8e95-547bc22aacb2, V: 0.2.1 )'
31.01.2018 01:12:27 -  with parameters: 
31.01.2018 01:12:27 - etf.testcases = *
31.01.2018 01:12:27 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
31.01.2018 01:12:27 - Setting state to CREATED
31.01.2018 01:12:27 - Changed state from CREATED to INITIALIZING
31.01.2018 01:12:27 - Starting TestRun.98e13495-c9a7-4893-8d08-51d20097b5ff at 2018-01-31T01:12:28+01:00
31.01.2018 01:12:28 - Changed state from INITIALIZING to INITIALIZED
31.01.2018 01:12:28 - TestRunTask initialized
31.01.2018 01:12:28 - Creating new tests databases to speed up tests.
31.01.2018 01:12:28 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 01:12:28 - Optimizing last database etf-tdb-72ec38a5-ec73-49ec-b8a1-b8216587a599-0 
31.01.2018 01:12:28 - Import completed
31.01.2018 01:12:30 - Validation ended with 0 error(s)
31.01.2018 01:12:30 - Compiling test script
31.01.2018 01:12:30 - Starting XQuery tests
31.01.2018 01:12:30 - "Testing 1 features"
31.01.2018 01:12:30 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-encoding/inspire-gml/ets-inspire-gml-bsxets.xml"
31.01.2018 01:12:30 - "Statistics table: 0 ms"
31.01.2018 01:12:30 - "Test Suite 'Conformance class: INSPIRE GML encoding' started"
31.01.2018 01:12:30 - "Test Case 'Basic tests' started"
31.01.2018 01:12:30 - "Test Assertion 'gml.a.1: Errors loading the XML documents': PASSED - 0 ms"
31.01.2018 01:12:30 - "Test Assertion 'gml.a.2: Document root element': PASSED - 0 ms"
31.01.2018 01:12:30 - "Test Assertion 'gml.a.3: Character encoding': NOT_APPLICABLE"
31.01.2018 01:12:30 - "Test Case 'Basic tests' finished: PASSED"
31.01.2018 01:12:30 - "Test Suite 'Conformance class: INSPIRE GML encoding' finished: PASSED"
31.01.2018 01:12:30 - Releasing resources
31.01.2018 01:12:30 - TestRunTask initialized
31.01.2018 01:12:30 - Recreating new tests databases as the Test Object has changed!
31.01.2018 01:12:30 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 01:12:30 - Optimizing last database etf-tdb-72ec38a5-ec73-49ec-b8a1-b8216587a599-0 
31.01.2018 01:12:30 - Import completed
31.01.2018 01:12:31 - Validation ended with 0 error(s)
31.01.2018 01:12:31 - Compiling test script
31.01.2018 01:12:31 - Starting XQuery tests
31.01.2018 01:12:31 - "Testing 1 features"
31.01.2018 01:12:31 - "Indexing features (parsing errors: 0): 47 ms"
31.01.2018 01:12:31 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/schemas/ets-schemas-bsxets.xml"
31.01.2018 01:12:31 - "Statistics table: 1 ms"
31.01.2018 01:12:31 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' started"
31.01.2018 01:12:31 - "Test Case 'Schema' started"
31.01.2018 01:12:31 - "Test Assertion 'gmlas.a.1: Mapping of source data to INSPIRE': PASSED_MANUAL"
31.01.2018 01:12:31 - "Test Assertion 'gmlas.a.2: Modelling of additional spatial object types': PASSED_MANUAL"
31.01.2018 01:12:31 - "Test Case 'Schema' finished: PASSED_MANUAL"
31.01.2018 01:12:31 - "Test Case 'Schema validation' started"
31.01.2018 01:12:31 - "Test Assertion 'gmlas.b.1: xsi:schemaLocation attribute': PASSED - 0 ms"
31.01.2018 01:12:31 - "Validating service.xml"
31.01.2018 01:12:37 - "Duration: 6703 ms. Errors: 0."
31.01.2018 01:12:37 - "Test Assertion 'gmlas.b.2: validate XML documents': PASSED - 6704 ms"
31.01.2018 01:12:37 - "Test Case 'Schema validation' finished: PASSED"
31.01.2018 01:12:37 - "Test Case 'GML model' started"
31.01.2018 01:12:37 - "Test Assertion 'gmlas.c.1: Consistency with the GML model': PASSED - 1 ms"
31.01.2018 01:12:37 - "Test Assertion 'gmlas.c.2: nilReason attributes require xsi:nil=true': PASSED - 0 ms"
31.01.2018 01:12:37 - "Test Assertion 'gmlas.c.3: nilReason values': PASSED - 0 ms"
31.01.2018 01:12:37 - "Test Case 'GML model' finished: PASSED"
31.01.2018 01:12:37 - "Test Case 'Simple features' started"
31.01.2018 01:12:37 - "Test Assertion 'gmlas.d.1: No spatial topology objects': PASSED - 0 ms"
31.01.2018 01:12:37 - "Test Assertion 'gmlas.d.2: No non-linear interpolation': PASSED - 0 ms"
31.01.2018 01:12:37 - "Test Assertion 'gmlas.d.3: Surface geometry elements': PASSED - 0 ms"
31.01.2018 01:12:37 - "Test Assertion 'gmlas.d.4: No non-planar interpolation': PASSED - 0 ms"
31.01.2018 01:12:37 - "Test Assertion 'gmlas.d.5: Geometry elements': PASSED - 0 ms"
31.01.2018 01:12:37 - "Test Assertion 'gmlas.d.6: Point coordinates in gml:pos': PASSED - 0 ms"
31.01.2018 01:12:37 - "Test Assertion 'gmlas.d.7: Curve/Surface coordinates in gml:posList': PASSED - 0 ms"
31.01.2018 01:12:37 - "Test Assertion 'gmlas.d.8: No array property elements': PASSED - 0 ms"
31.01.2018 01:12:37 - "Test Assertion 'gmlas.d.9: 1, 2 or 3 coordinate dimensions': PASSED - 0 ms"
31.01.2018 01:12:37 - "Test Assertion 'gmlas.d.10: Validate geometries (1)': PASSED - 0 ms"
31.01.2018 01:12:37 - "Test Assertion 'gmlas.d.11: Validate geometries (2)': PASSED - 0 ms"
31.01.2018 01:12:37 - "Test Case 'Simple features' finished: PASSED"
31.01.2018 01:12:37 - "Test Case 'Code list values in basic data types' started"
31.01.2018 01:12:37 - "Test Assertion 'gmlas.e.1: GrammaticalNumber attributes': PASSED - 38 ms"
31.01.2018 01:12:37 - "Test Assertion 'gmlas.e.2: GrammaticalGender attributes': PASSED - 6 ms"
31.01.2018 01:12:37 - "Test Assertion 'gmlas.e.3: NameStatus attributes': PASSED - 4 ms"
31.01.2018 01:12:37 - "Test Assertion 'gmlas.e.4: Nativeness attributes': PASSED - 5 ms"
31.01.2018 01:12:37 - "Test Case 'Code list values in basic data types' finished: PASSED"
31.01.2018 01:12:37 - "Test Case 'Constraints' started"
31.01.2018 01:12:37 - "Test Assertion 'gmlas.f.1: At least one of the two attributes pronunciationSoundLink and pronunciationIPA shall not be void': PASSED - 1 ms"
31.01.2018 01:12:37 - "Test Case 'Constraints' finished: PASSED"
31.01.2018 01:12:37 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' finished: PASSED_MANUAL"
31.01.2018 01:12:38 - Releasing resources
31.01.2018 01:12:38 - TestRunTask initialized
31.01.2018 01:12:38 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 01:12:38 - Validation ended with 0 error(s)
31.01.2018 01:12:38 - Compiling test script
31.01.2018 01:12:38 - Starting XQuery tests
31.01.2018 01:12:38 - "Testing 1 features"
31.01.2018 01:12:38 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-cp/cp-gml/ets-cp-gml-bsxets.xml"
31.01.2018 01:12:38 - "Statistics table: 0 ms"
31.01.2018 01:12:38 - "Test Suite 'Conformance class: GML application schemas, Cadastral Parcels' started"
31.01.2018 01:12:38 - "Test Case 'Basic test' started"
31.01.2018 01:12:38 - "Test Assertion 'cp-gml.a.1: CadastralParcel feature in dataset': FAILED - 0 ms"
31.01.2018 01:12:38 - "Test Case 'Basic test' finished: FAILED"
31.01.2018 01:12:38 - "Test Suite 'Conformance class: GML application schemas, Cadastral Parcels' finished: FAILED"
31.01.2018 01:12:38 - Releasing resources
31.01.2018 01:12:38 - TestRunTask initialized
31.01.2018 01:12:38 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 01:12:38 - Validation ended with 0 error(s)
31.01.2018 01:12:38 - Compiling test script
31.01.2018 01:12:38 - Starting XQuery tests
31.01.2018 01:12:38 - "Testing 1 features"
31.01.2018 01:12:39 - "Indexing features (parsing errors: 0): 43 ms"
31.01.2018 01:12:39 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-cp/cp-as/ets-cp-as-bsxets.xml"
31.01.2018 01:12:39 - "Statistics table: 0 ms"
31.01.2018 01:12:39 - "Test Suite 'Conformance class: Application Schema, Cadastral parcel' started"
31.01.2018 01:12:39 - "Test Case 'Modelling of object references' started"
31.01.2018 01:12:39 - "Test Assertion 'cp-as.a.1: Verify whether all instances of the spatial object type CadastralParcel carry as a thematic identifier the attribute nationalCadastralReference.': PASSED - 0 ms"
31.01.2018 01:12:39 - "Test Assertion 'cp-as.a.2: Manual review - Verify that the value of the attribute nationalCadastralReference enables the users to make the link with rights, owners and other cadastral information in national cadastral registers or equivalent.': PASSED_MAN..."
31.01.2018 01:12:39 - "Test Case 'Modelling of object references' finished: PASSED_MANUAL"
31.01.2018 01:12:39 - "Test Case 'Cadastral Boundaries' started"
31.01.2018 01:12:39 - "Test Assertion 'cp-as.b.1: Cadastral boundaries corresponding to the outline of a cadastral parcel form closed ring(s).': NOT_APPLICABLE"
31.01.2018 01:12:39 - "Test Case 'Cadastral Boundaries' finished: NOT_APPLICABLE"
31.01.2018 01:12:39 - "Test Case 'Code list values' started"
31.01.2018 01:12:39 - "Test Assertion 'cp-as.c.1: CadastralZoningLevel attributes': PASSED - 6 ms"
31.01.2018 01:12:39 - "Test Case 'Code list values' finished: PASSED"
31.01.2018 01:12:39 - "Test Case 'Constraints' started"
31.01.2018 01:12:39 - "Test Assertion 'cp-as.d.1: Value of areaValue shall be given in square meters': PASSED - 0 ms"
31.01.2018 01:12:39 - "Test Assertion 'cp-as.d.2: Value of estimatedAccuracy shall be given in meters': FAILED - 1 ms"
31.01.2018 01:12:39 - "Test Assertion 'cp-as.d.3: Type of geometry shall be GM_Surface or GM_MultiSurface': PASSED - 0 ms"
31.01.2018 01:12:39 - "Test Assertion 'cp-as.d.4: A lower level cadastral zoning shall be part of an upper level zoning.': PASSED - 0 ms"
31.01.2018 01:12:39 - "Test Case 'Constraints' finished: FAILED"
31.01.2018 01:12:39 - "Test Suite 'Conformance class: Application Schema, Cadastral parcel' finished: FAILED"
31.01.2018 01:12:39 - Releasing resources
31.01.2018 01:12:39 - TestRunTask initialized
31.01.2018 01:12:39 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 01:12:39 - Validation ended with 0 error(s)
31.01.2018 01:12:39 - Compiling test script
31.01.2018 01:12:39 - Starting XQuery tests
31.01.2018 01:12:39 - "Testing 1 features"
31.01.2018 01:12:39 - "Indexing features (parsing errors: 0): 41 ms"
31.01.2018 01:12:39 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/data-consistency/ets-data-consistency-bsxets.xml"
31.01.2018 01:12:39 - "Statistics table: 0 ms"
31.01.2018 01:12:39 - "Test Suite 'Conformance class: Data consistency, General requirements' started"
31.01.2018 01:12:39 - "Test Case 'Version consistency' started"
31.01.2018 01:12:39 - "Test Assertion 'dc.a.1: Version lifespan plausible': PASSED - 1 ms"
31.01.2018 01:12:39 - "Test Assertion 'dc.a.2: Unique identifier persistency': PASSED_MANUAL"
31.01.2018 01:12:39 - "Test Assertion 'dc.a.3: Spatial object type stable': PASSED_MANUAL"
31.01.2018 01:12:39 - "Test Case 'Version consistency' finished: PASSED_MANUAL"
31.01.2018 01:12:39 - "Test Case 'Temporal consistency' started"
31.01.2018 01:12:39 - "Test Assertion 'dc.b.1: Valid time plausible': PASSED - 0 ms"
31.01.2018 01:12:39 - "Test Case 'Temporal consistency' finished: PASSED"
31.01.2018 01:12:39 - "Test Suite 'Conformance class: Data consistency, General requirements' finished: PASSED_MANUAL"
31.01.2018 01:12:40 - Releasing resources
31.01.2018 01:12:40 - TestRunTask initialized
31.01.2018 01:12:40 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 01:12:40 - Validation ended with 0 error(s)
31.01.2018 01:12:40 - Compiling test script
31.01.2018 01:12:40 - Starting XQuery tests
31.01.2018 01:12:40 - "Testing 1 features"
31.01.2018 01:12:40 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-cp/cp-dc/ets-cp-dc-bsxets.xml"
31.01.2018 01:12:40 - "Statistics table: 0 ms"
31.01.2018 01:12:40 - "Test Suite 'Conformance class: Data consistency, Cadastral Parcels' started"
31.01.2018 01:12:40 - "Test Case 'Additional theme-specific consistency rules' started"
31.01.2018 01:12:40 - "Test Assertion 'cp-dc.a.1: Test always passes': PASSED - 0 ms"
31.01.2018 01:12:40 - "Test Case 'Additional theme-specific consistency rules' finished: PASSED"
31.01.2018 01:12:40 - "Test Suite 'Conformance class: Data consistency, Cadastral Parcels' finished: PASSED"
31.01.2018 01:12:40 - Releasing resources
31.01.2018 01:12:40 - TestRunTask initialized
31.01.2018 01:12:40 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 01:12:40 - Validation ended with 0 error(s)
31.01.2018 01:12:40 - Compiling test script
31.01.2018 01:12:40 - Starting XQuery tests
31.01.2018 01:12:40 - "Testing 1 features"
31.01.2018 01:12:40 - "Indexing features (parsing errors: 0): 41 ms"
31.01.2018 01:12:40 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/information-accessibility/ets-information-accessibility-bsxets.xml"
31.01.2018 01:12:40 - "Statistics table: 0 ms"
31.01.2018 01:12:40 - "Test Suite 'Conformance class: Information accessibility, General requirements' started"
31.01.2018 01:12:40 - "Test Case 'Coordinate reference systems (CRS)' started"
31.01.2018 01:12:40 - "Test Assertion 'ia.a.1: CRS publicly accessible via HTTP': FAILED - 1 ms"
31.01.2018 01:12:40 - "Test Case 'Coordinate reference systems (CRS)' finished: FAILED"
31.01.2018 01:12:40 - "Test Suite 'Conformance class: Information accessibility, General requirements' finished: FAILED"
31.01.2018 01:12:41 - Releasing resources
31.01.2018 01:12:41 - TestRunTask initialized
31.01.2018 01:12:41 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 01:12:41 - Validation ended with 0 error(s)
31.01.2018 01:12:41 - Compiling test script
31.01.2018 01:12:41 - Starting XQuery tests
31.01.2018 01:12:41 - "Testing 1 features"
31.01.2018 01:12:41 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-cp/cp-ia/ets-cp-ia-bsxets.xml"
31.01.2018 01:12:41 - "Statistics table: 0 ms"
31.01.2018 01:12:41 - "Test Suite 'Conformance class: Information accessibility, Cadastral Parcels' started"
31.01.2018 01:12:41 - "Test Case 'Code lists' started"
31.01.2018 01:12:41 - "Test Assertion 'cp-ia.a.1: Code list extensions accessible': PASSED - 0 ms"
31.01.2018 01:12:41 - "Test Case 'Code lists' finished: PASSED"
31.01.2018 01:12:41 - "Test Case 'Feature references' started"
31.01.2018 01:12:41 - "Test Assertion 'cp-ia.b.1: BasicPropertyUnit.AdministrativeUnit and CadastralParcel.AdministrativeUnit': PASSED - 0 ms"
31.01.2018 01:12:41 - "Test Assertion 'cp-ia.b.2: CadastralBoundary.parcel': PASSED - 1 ms"
31.01.2018 01:12:41 - "Test Assertion 'cp-ia.b.3: CadastralParcel.basicPropertyUnit': PASSED - 0 ms"
31.01.2018 01:12:41 - "Test Assertion 'cp-ia.b.4: CadastralParcel.zoning': PASSED - 0 ms"
31.01.2018 01:12:41 - "Test Assertion 'cp-ia.b.5: CadastralZoning.upperLevelUnit': PASSED - 0 ms"
31.01.2018 01:12:41 - "Test Case 'Feature references' finished: PASSED"
31.01.2018 01:12:41 - "Test Suite 'Conformance class: Information accessibility, Cadastral Parcels' finished: PASSED"
31.01.2018 01:12:41 - Releasing resources
31.01.2018 01:12:41 - TestRunTask initialized
31.01.2018 01:12:41 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 01:12:41 - Validation ended with 0 error(s)
31.01.2018 01:12:41 - Compiling test script
31.01.2018 01:12:41 - Starting XQuery tests
31.01.2018 01:12:41 - "Testing 1 features"
31.01.2018 01:12:41 - "Indexing features (parsing errors: 0): 42 ms"
31.01.2018 01:12:41 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/reference-systems/ets-reference-systems-bsxets.xml"
31.01.2018 01:12:41 - "Statistics table: 0 ms"
31.01.2018 01:12:41 - "Test Suite 'Conformance class: Reference systems, General requirements' started"
31.01.2018 01:12:41 - "Test Case 'Spatial reference systems' started"
31.01.2018 01:12:41 - "Test Assertion 'rs.a.1: Spatial reference systems in feature geometries': FAILED - 1 ms"
31.01.2018 01:12:41 - "Test Assertion 'rs.a.2: Default spatial reference systems in feature collections': PASSED - 0 ms"
31.01.2018 01:12:41 - "Test Case 'Spatial reference systems' finished: FAILED"
31.01.2018 01:12:41 - "Test Case 'Temporal reference systems' started"
31.01.2018 01:12:41 - "Test Assertion 'rs.a.3: Temporal reference systems in features': PASSED - 0 ms"
31.01.2018 01:12:41 - "Test Case 'Temporal reference systems' finished: PASSED"
31.01.2018 01:12:41 - "Test Suite 'Conformance class: Reference systems, General requirements' finished: FAILED"
31.01.2018 01:12:42 - Releasing resources
31.01.2018 01:12:42 - TestRunTask initialized
31.01.2018 01:12:42 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 01:12:42 - Validation ended with 0 error(s)
31.01.2018 01:12:42 - Compiling test script
31.01.2018 01:12:42 - Starting XQuery tests
31.01.2018 01:12:42 - "Testing 1 features"
31.01.2018 01:12:42 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-cp/cp-rs/ets-cp-rs-bsxets.xml"
31.01.2018 01:12:42 - "Statistics table: 0 ms"
31.01.2018 01:12:42 - "Test Suite 'Conformance class: Reference Systems, Cadastral Parcels' started"
31.01.2018 01:12:42 - "Test Case 'Lambert Conformal Conic projection' started"
31.01.2018 01:12:42 - "Test Assertion 'cp-rs.a.1: Verify that data related to the spatial data theme Cadastral Parcels made available in plane coordinates using the Lambert Conformal Conic projection are also available in at least one other of the coordinate reference systems s..."
31.01.2018 01:12:42 - "Test Assertion 'cp-rs.a.2: Verify that if there is a reference to the Lambert Conformal Conic projection in the bounding box of the feature collection, there is also a reference to (at least) one other of the coordinate reference systems specified in sect..."
31.01.2018 01:12:42 - "Test Case 'Lambert Conformal Conic projection' finished: PASSED"
31.01.2018 01:12:42 - "Test Suite 'Conformance class: Reference Systems, Cadastral Parcels' finished: PASSED"
31.01.2018 01:12:42 - Releasing resources
31.01.2018 01:12:42 - Changed state from INITIALIZED to RUNNING
31.01.2018 01:12:42 - Duration: 15sec
31.01.2018 01:12:42 - TestRun finished
31.01.2018 01:12:42 - Changed state from RUNNING to COMPLETED
