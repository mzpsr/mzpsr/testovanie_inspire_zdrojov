11.12.2017 21:54:52 - Preparing Test Run metadata_17058520_&Scaron;t&aacute;tna ochrana pr&iacute;rody Slovenskej republiky_INSPIRE Ukladacia služba &ndash; Chr&aacute;nen&eacute; &uacute;zemia (initiated Mon Dec 11 21:54:52 CET 2017)
11.12.2017 21:54:52 - Resolving Executable Test Suite dependencies
11.12.2017 21:54:52 - Preparing 2 Test Task:
11.12.2017 21:54:52 -  TestTask 1 (f8ec6616-9dbd-435d-9af5-68dcb8a84a1c)
11.12.2017 21:54:52 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
11.12.2017 21:54:52 -  with parameters: 
11.12.2017 21:54:52 - etf.testcases = *
11.12.2017 21:54:52 -  TestTask 2 (867891dc-3c0f-47cf-b661-05f7cb3b3c63)
11.12.2017 21:54:52 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.5 )'
11.12.2017 21:54:52 -  with parameters: 
11.12.2017 21:54:52 - etf.testcases = *
11.12.2017 21:54:52 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
11.12.2017 21:54:52 - Setting state to CREATED
11.12.2017 21:54:52 - Changed state from CREATED to INITIALIZING
11.12.2017 21:54:52 - Starting TestRun.e2b07ab4-3a0c-41c4-ad17-23db9c952936 at 2017-12-11T21:54:54+01:00
11.12.2017 21:54:54 - Changed state from INITIALIZING to INITIALIZED
11.12.2017 21:54:54 - TestRunTask initialized
11.12.2017 21:54:54 - Creating new tests databases to speed up tests.
11.12.2017 21:54:54 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
11.12.2017 21:54:54 - Optimizing last database etf-tdb-ae0d38d0-ce40-4bf6-b1c5-e596cd760162-0 
11.12.2017 21:54:54 - Import completed
11.12.2017 21:54:55 - Validation ended with 0 error(s)
11.12.2017 21:54:55 - Compiling test script
11.12.2017 21:54:55 - Starting XQuery tests
11.12.2017 21:54:55 - "Testing 1 records"
11.12.2017 21:54:55 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
11.12.2017 21:54:55 - "Statistics table: 0 ms"
11.12.2017 21:54:55 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
11.12.2017 21:54:55 - "Test Case 'Schema validation' started"
11.12.2017 21:54:57 - "Validating file GetRecordByIdResponse.xml: 2230 ms"
11.12.2017 21:54:57 - "Test Assertion 'md-xml.a.1: Validate XML documents': PASSED - 2231 ms"
11.12.2017 21:54:57 - "Test Case 'Schema validation' finished: PASSED"
11.12.2017 21:54:57 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: PASSED"
11.12.2017 21:54:57 - Releasing resources
11.12.2017 21:54:57 - TestRunTask initialized
11.12.2017 21:54:57 - Recreating new tests databases as the Test Object has changed!
11.12.2017 21:54:57 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
11.12.2017 21:54:57 - Optimizing last database etf-tdb-ae0d38d0-ce40-4bf6-b1c5-e596cd760162-0 
11.12.2017 21:54:57 - Import completed
11.12.2017 21:54:57 - Validation ended with 0 error(s)
11.12.2017 21:54:57 - Compiling test script
11.12.2017 21:54:58 - Starting XQuery tests
11.12.2017 21:54:58 - "Testing 1 records"
11.12.2017 21:54:58 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
11.12.2017 21:54:58 - "Statistics table: 0 ms"
11.12.2017 21:54:58 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
11.12.2017 21:54:58 - "Test Case 'Common tests' started"
11.12.2017 21:54:58 - "Test Assertion 'md-iso.a.1: Title': PASSED - 0 ms"
11.12.2017 21:54:58 - "Test Assertion 'md-iso.a.2: Abstract': PASSED - 0 ms"
11.12.2017 21:54:58 - "Test Assertion 'md-iso.a.3: Access and use conditions': PASSED_MANUAL - 0 ms"
11.12.2017 21:54:58 - "Test Assertion 'md-iso.a.4: Public access': PASSED - 0 ms"
11.12.2017 21:54:58 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 0 ms"
11.12.2017 21:54:58 - "Test Assertion 'md-iso.a.6: Language': PASSED - 0 ms"
11.12.2017 21:54:58 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 1 ms"
11.12.2017 21:54:58 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
11.12.2017 21:54:58 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 0 ms"
11.12.2017 21:54:58 - "Test Assertion 'md-iso.a.10: Responsible party contact info': PASSED - 0 ms"
11.12.2017 21:54:58 - "Test Assertion 'md-iso.a.11: Responsible party role': PASSED - 0 ms"
11.12.2017 21:54:58 - "Test Case 'Common tests' finished: PASSED_MANUAL"
11.12.2017 21:54:58 - "Test Case 'Hierarchy level' started"
11.12.2017 21:54:58 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 0 ms"
11.12.2017 21:54:58 - "Test Case 'Hierarchy level' finished: PASSED"
11.12.2017 21:54:58 - "Test Case 'Dataset (series) tests' started"
11.12.2017 21:54:58 - "Test Assertion 'md-iso.c.1: Dataset identification': PASSED - 1 ms"
11.12.2017 21:54:58 - "Test Assertion 'md-iso.c.2: Dataset language': PASSED - 0 ms"
11.12.2017 21:54:58 - "Test Assertion 'md-iso.c.3: Dataset linkage': PASSED - 0 ms"
11.12.2017 21:54:58 - "Test Assertion 'md-iso.c.4: Dataset conformity': PASSED - 0 ms"
11.12.2017 21:54:58 - "Test Assertion 'md-iso.c.5: Dataset topic': PASSED - 1 ms"
11.12.2017 21:54:58 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': PASSED - 0 ms"
11.12.2017 21:54:58 - "Test Assertion 'md-iso.c.7: Dataset lineage': PASSED - 0 ms"
11.12.2017 21:54:58 - "Test Case 'Dataset (series) tests' finished: PASSED"
11.12.2017 21:54:58 - "Test Case 'Service tests' started"
11.12.2017 21:54:58 - "Test Assertion 'md-iso.d.1: Service type': PASSED - 0 ms"
11.12.2017 21:54:58 - "Checking URL: 'http://inspire.biomonitoring.sk/geoserver/wfs'"
11.12.2017 21:54:58 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED_MANUAL - 373 ms"
11.12.2017 21:54:58 - "Checking URL: 'https://rpi.gov.sk/rpi_csw/service.svc/get?request=GetRecordById&amp;service=CSW&amp;version=2.0.2&amp;elementSetName=full&amp;outputschema=http://www.isotc211.org/2005/gmd&amp;Id=https://data.gov.sk/set/rpi/gmd/17058520/5E8F5C776C9335D7E05..."
11.12.2017 21:55:00 - "Test Assertion 'md-iso.d.3: Coupled resource': PASSED - 2181 ms"
11.12.2017 21:55:00 - "Test Case 'Service tests' finished: PASSED_MANUAL"
11.12.2017 21:55:00 - "Test Case 'Keywords' started"
11.12.2017 21:55:00 - "Test Assertion 'md-iso.e.1: Keywords': PASSED - 0 ms"
11.12.2017 21:55:00 - "Test Case 'Keywords' finished: PASSED"
11.12.2017 21:55:00 - "Test Case 'Keywords - details' started"
11.12.2017 21:55:00 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 1 ms"
11.12.2017 21:55:00 - "Checking URL: 'http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/SpatialDataServiceCategory.en.atom'"
11.12.2017 21:55:02 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 1444 ms"
11.12.2017 21:55:02 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 0 ms"
11.12.2017 21:55:02 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 0 ms"
11.12.2017 21:55:02 - "Test Case 'Keywords - details' finished: PASSED"
11.12.2017 21:55:02 - "Test Case 'Temporal extent' started"
11.12.2017 21:55:02 - "Test Assertion 'md-iso.g.1: Temporal extent': PASSED - 0 ms"
11.12.2017 21:55:02 - "Test Case 'Temporal extent' finished: PASSED"
11.12.2017 21:55:02 - "Test Case 'Temporal extent - details' started"
11.12.2017 21:55:02 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 0 ms"
11.12.2017 21:55:02 - "Test Case 'Temporal extent - details' finished: PASSED"
11.12.2017 21:55:02 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: PASSED_MANUAL"
11.12.2017 21:55:02 - Releasing resources
11.12.2017 21:55:02 - Changed state from INITIALIZED to RUNNING
11.12.2017 21:55:02 - Duration: 9sec
11.12.2017 21:55:02 - TestRun finished
11.12.2017 21:55:02 - Changed state from RUNNING to COMPLETED
