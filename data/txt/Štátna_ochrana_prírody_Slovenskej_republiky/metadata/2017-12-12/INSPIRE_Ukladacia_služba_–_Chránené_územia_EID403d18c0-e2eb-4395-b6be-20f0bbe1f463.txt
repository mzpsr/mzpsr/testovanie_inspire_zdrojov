12.12.2017 01:10:05 - Preparing Test Run metadata_17058520_&Scaron;t&aacute;tna ochrana pr&iacute;rody Slovenskej republiky_INSPIRE Ukladacia služba &ndash; Chr&aacute;nen&eacute; &uacute;zemia (initiated Tue Dec 12 01:10:05 CET 2017)
12.12.2017 01:10:05 - Resolving Executable Test Suite dependencies
12.12.2017 01:10:05 - Preparing 2 Test Task:
12.12.2017 01:10:05 -  TestTask 1 (e53b9480-fd4b-4a05-a0a1-8bb7bfaa15c2)
12.12.2017 01:10:05 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
12.12.2017 01:10:05 -  with parameters: 
12.12.2017 01:10:05 - etf.testcases = *
12.12.2017 01:10:05 -  TestTask 2 (8a467a87-094b-4c1c-9dbb-603e85b0a1fb)
12.12.2017 01:10:05 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.5 )'
12.12.2017 01:10:05 -  with parameters: 
12.12.2017 01:10:05 - etf.testcases = *
12.12.2017 01:10:05 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
12.12.2017 01:10:05 - Setting state to CREATED
12.12.2017 01:10:05 - Changed state from CREATED to INITIALIZING
12.12.2017 01:10:05 - Starting TestRun.403d18c0-e2eb-4395-b6be-20f0bbe1f463 at 2017-12-12T01:10:06+01:00
12.12.2017 01:10:06 - Changed state from INITIALIZING to INITIALIZED
12.12.2017 01:10:06 - TestRunTask initialized
12.12.2017 01:10:06 - Creating new tests databases to speed up tests.
12.12.2017 01:10:06 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 01:10:06 - Optimizing last database etf-tdb-65310315-1d11-48a2-8713-0c80706eff80-0 
12.12.2017 01:10:06 - Import completed
12.12.2017 01:10:06 - Validation ended with 0 error(s)
12.12.2017 01:10:06 - Compiling test script
12.12.2017 01:10:06 - Starting XQuery tests
12.12.2017 01:10:07 - "Testing 1 records"
12.12.2017 01:10:07 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
12.12.2017 01:10:07 - "Statistics table: 0 ms"
12.12.2017 01:10:07 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
12.12.2017 01:10:07 - "Test Case 'Schema validation' started"
12.12.2017 01:10:09 - "Validating file GetRecordByIdResponse.xml: 2297 ms"
12.12.2017 01:10:09 - "Test Assertion 'md-xml.a.1: Validate XML documents': PASSED - 2298 ms"
12.12.2017 01:10:09 - "Test Case 'Schema validation' finished: PASSED"
12.12.2017 01:10:09 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: PASSED"
12.12.2017 01:10:09 - Releasing resources
12.12.2017 01:10:09 - TestRunTask initialized
12.12.2017 01:10:09 - Recreating new tests databases as the Test Object has changed!
12.12.2017 01:10:09 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 01:10:09 - Optimizing last database etf-tdb-65310315-1d11-48a2-8713-0c80706eff80-0 
12.12.2017 01:10:09 - Import completed
12.12.2017 01:10:09 - Validation ended with 0 error(s)
12.12.2017 01:10:09 - Compiling test script
12.12.2017 01:10:09 - Starting XQuery tests
12.12.2017 01:10:09 - "Testing 1 records"
12.12.2017 01:10:09 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
12.12.2017 01:10:09 - "Statistics table: 1 ms"
12.12.2017 01:10:09 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
12.12.2017 01:10:09 - "Test Case 'Common tests' started"
12.12.2017 01:10:09 - "Test Assertion 'md-iso.a.1: Title': PASSED - 0 ms"
12.12.2017 01:10:09 - "Test Assertion 'md-iso.a.2: Abstract': PASSED - 0 ms"
12.12.2017 01:10:09 - "Test Assertion 'md-iso.a.3: Access and use conditions': PASSED_MANUAL - 1 ms"
12.12.2017 01:10:09 - "Test Assertion 'md-iso.a.4: Public access': PASSED - 0 ms"
12.12.2017 01:10:09 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 0 ms"
12.12.2017 01:10:09 - "Test Assertion 'md-iso.a.6: Language': PASSED - 0 ms"
12.12.2017 01:10:09 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 0 ms"
12.12.2017 01:10:09 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
12.12.2017 01:10:09 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 0 ms"
12.12.2017 01:10:09 - "Test Assertion 'md-iso.a.10: Responsible party contact info': PASSED - 0 ms"
12.12.2017 01:10:09 - "Test Assertion 'md-iso.a.11: Responsible party role': PASSED - 0 ms"
12.12.2017 01:10:09 - "Test Case 'Common tests' finished: PASSED_MANUAL"
12.12.2017 01:10:09 - "Test Case 'Hierarchy level' started"
12.12.2017 01:10:09 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 0 ms"
12.12.2017 01:10:09 - "Test Case 'Hierarchy level' finished: PASSED"
12.12.2017 01:10:09 - "Test Case 'Dataset (series) tests' started"
12.12.2017 01:10:09 - "Test Assertion 'md-iso.c.1: Dataset identification': PASSED - 0 ms"
12.12.2017 01:10:09 - "Test Assertion 'md-iso.c.2: Dataset language': PASSED - 0 ms"
12.12.2017 01:10:09 - "Test Assertion 'md-iso.c.3: Dataset linkage': PASSED - 0 ms"
12.12.2017 01:10:09 - "Test Assertion 'md-iso.c.4: Dataset conformity': PASSED - 0 ms"
12.12.2017 01:10:09 - "Test Assertion 'md-iso.c.5: Dataset topic': PASSED - 0 ms"
12.12.2017 01:10:09 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': PASSED - 0 ms"
12.12.2017 01:10:09 - "Test Assertion 'md-iso.c.7: Dataset lineage': PASSED - 0 ms"
12.12.2017 01:10:09 - "Test Case 'Dataset (series) tests' finished: PASSED"
12.12.2017 01:10:09 - "Test Case 'Service tests' started"
12.12.2017 01:10:09 - "Test Assertion 'md-iso.d.1: Service type': PASSED - 0 ms"
12.12.2017 01:10:09 - "Checking URL: 'http://inspire.biomonitoring.sk/geoserver/wfs'"
12.12.2017 01:10:10 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED_MANUAL - 227 ms"
12.12.2017 01:10:10 - "Checking URL: 'https://rpi.gov.sk/rpi_csw/service.svc/get?request=GetRecordById&amp;service=CSW&amp;version=2.0.2&amp;elementSetName=full&amp;outputschema=http://www.isotc211.org/2005/gmd&amp;Id=https://data.gov.sk/set/rpi/gmd/17058520/5E8F5C776C9335D7E05..."
12.12.2017 01:10:12 - "Test Assertion 'md-iso.d.3: Coupled resource': PASSED - 2377 ms"
12.12.2017 01:10:12 - "Test Case 'Service tests' finished: PASSED_MANUAL"
12.12.2017 01:10:12 - "Test Case 'Keywords' started"
12.12.2017 01:10:12 - "Test Assertion 'md-iso.e.1: Keywords': PASSED - 0 ms"
12.12.2017 01:10:12 - "Test Case 'Keywords' finished: PASSED"
12.12.2017 01:10:12 - "Test Case 'Keywords - details' started"
12.12.2017 01:10:12 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 0 ms"
12.12.2017 01:10:12 - "Checking URL: 'http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/SpatialDataServiceCategory.en.atom'"
12.12.2017 01:10:12 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 32 ms"
12.12.2017 01:10:12 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 1 ms"
12.12.2017 01:10:12 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 0 ms"
12.12.2017 01:10:12 - "Test Case 'Keywords - details' finished: PASSED"
12.12.2017 01:10:12 - "Test Case 'Temporal extent' started"
12.12.2017 01:10:12 - "Test Assertion 'md-iso.g.1: Temporal extent': PASSED - 1 ms"
12.12.2017 01:10:12 - "Test Case 'Temporal extent' finished: PASSED"
12.12.2017 01:10:12 - "Test Case 'Temporal extent - details' started"
12.12.2017 01:10:12 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 0 ms"
12.12.2017 01:10:12 - "Test Case 'Temporal extent - details' finished: PASSED"
12.12.2017 01:10:12 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: PASSED_MANUAL"
12.12.2017 01:10:12 - Releasing resources
12.12.2017 01:10:12 - Changed state from INITIALIZED to RUNNING
12.12.2017 01:10:12 - Duration: 7sec
12.12.2017 01:10:12 - TestRun finished
12.12.2017 01:10:12 - Changed state from RUNNING to COMPLETED
