15.11.2017 22:58:47 - Preparing Test Run tn-ra:RailwayArea (initiated Wed Nov 15 22:58:47 CET 2017)
15.11.2017 22:58:47 - Resolving Executable Test Suite dependencies
15.11.2017 22:58:47 - Preparing 10 Test Task:
15.11.2017 22:58:47 -  TestTask 1 (4ca5ea77-2c4a-42c2-ac7e-113e02f4f040)
15.11.2017 22:58:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: GML application schemas, Transport Networks (EID: 9af1c865-1cf0-43ff-9250-069df01b0948, V: 0.2.1 )'
15.11.2017 22:58:47 -  with parameters: 
15.11.2017 22:58:47 - etf.testcases = *
15.11.2017 22:58:47 -  TestTask 2 (03ebb1e9-073b-400b-88b3-c977caea0cf6)
15.11.2017 22:58:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.4441cbde-371f-4899-90b3-145f4fd08ebc'
15.11.2017 22:58:47 -  with parameters: 
15.11.2017 22:58:47 - etf.testcases = *
15.11.2017 22:58:47 -  TestTask 3 (816d4d5a-7d2d-44c8-beef-f7ad80bb5215)
15.11.2017 22:58:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Application schema, Rail Transport Networks (EID: e2610a9f-6432-489d-8238-92b1193e7a3d, V: 0.2.1 )'
15.11.2017 22:58:47 -  with parameters: 
15.11.2017 22:58:47 - etf.testcases = *
15.11.2017 22:58:47 -  TestTask 4 (a60f870b-e429-424f-8aab-c0dfc968650e)
15.11.2017 22:58:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML encoding (EID: 545f9e49-009b-4114-9333-7ca26413b5d4, V: 0.2.1 )'
15.11.2017 22:58:47 -  with parameters: 
15.11.2017 22:58:47 - etf.testcases = *
15.11.2017 22:58:47 -  TestTask 5 (e9494c25-9fe2-4d3d-b189-98596ea3cd78)
15.11.2017 22:58:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
15.11.2017 22:58:47 -  with parameters: 
15.11.2017 22:58:47 - etf.testcases = *
15.11.2017 22:58:47 -  TestTask 6 (925f3d79-483d-4dc4-93c8-dd76a0680a2b)
15.11.2017 22:58:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Data consistency, Transport Networks (EID: 733af9a0-312b-4f71-9aa2-977cd2134d23, V: 0.2.2 )'
15.11.2017 22:58:47 -  with parameters: 
15.11.2017 22:58:47 - etf.testcases = *
15.11.2017 22:58:47 -  TestTask 7 (2bc43492-a807-4558-8792-57c040fd6bf1)
15.11.2017 22:58:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
15.11.2017 22:58:47 -  with parameters: 
15.11.2017 22:58:47 - etf.testcases = *
15.11.2017 22:58:47 -  TestTask 8 (0f2f75fa-554f-4fa1-85ff-c91a9ac8ee80)
15.11.2017 22:58:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Transport Networks (EID: df5db9a4-b15f-4193-a6ff-6e9951af46f5, V: 0.2.1 )'
15.11.2017 22:58:47 -  with parameters: 
15.11.2017 22:58:47 - etf.testcases = *
15.11.2017 22:58:47 -  TestTask 9 (82e17998-5bf5-408d-ba33-1f46ef6f1a3f)
15.11.2017 22:58:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
15.11.2017 22:58:47 -  with parameters: 
15.11.2017 22:58:47 - etf.testcases = *
15.11.2017 22:58:47 -  TestTask 10 (1578b173-78f5-4565-81a0-8a0fcf7a1730)
15.11.2017 22:58:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Reference systems, Transport Networks (EID: 9d35024d-9dd7-43a9-afff-d5aea5f51595, V: 0.2.0 )'
15.11.2017 22:58:47 -  with parameters: 
15.11.2017 22:58:47 - etf.testcases = *
15.11.2017 22:58:47 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
15.11.2017 22:58:47 - Setting state to CREATED
15.11.2017 22:58:47 - Changed state from CREATED to INITIALIZING
15.11.2017 22:58:47 - Starting TestRun.2cd3dd17-fbc7-42a1-af3e-8c0386c6f894 at 2017-11-15T22:58:49+01:00
15.11.2017 22:58:49 - Changed state from INITIALIZING to INITIALIZED
15.11.2017 22:58:49 - TestRunTask initialized
15.11.2017 22:58:49 - Creating new tests databases to speed up tests.
15.11.2017 22:58:49 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 22:58:49 - Optimizing last database etf-tdb-415ee868-c186-4773-874c-0d06bf0e330e-0 
15.11.2017 22:58:49 - Import completed
15.11.2017 22:58:50 - Validation ended with 0 error(s)
15.11.2017 22:58:50 - Compiling test script
15.11.2017 22:58:50 - Starting XQuery tests
15.11.2017 22:58:50 - "Testing 1 features"
15.11.2017 22:58:50 - "Indexing features (parsing errors: 0): 166 ms"
15.11.2017 22:58:50 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-gml/ets-tn-gml-bsxets.xml"
15.11.2017 22:58:50 - "Statistics table: 0 ms"
15.11.2017 22:58:50 - "Test Suite 'Conformance class: GML application schemas, Transport Networks' started"
15.11.2017 22:58:50 - "Test Case 'Basic test' started"
15.11.2017 22:58:50 - "Test Assertion 'tn-gml.a.1: Transport Network feature in dataset': PASSED - 0 ms"
15.11.2017 22:58:50 - "Test Case 'Basic test' finished: PASSED"
15.11.2017 22:58:50 - "Test Suite 'Conformance class: GML application schemas, Transport Networks' finished: PASSED"
15.11.2017 22:58:50 - Releasing resources
15.11.2017 22:58:50 - TestRunTask initialized
15.11.2017 22:58:50 - Recreating new tests databases as the Test Object has changed!
15.11.2017 22:58:50 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 22:58:50 - Optimizing last database etf-tdb-415ee868-c186-4773-874c-0d06bf0e330e-0 
15.11.2017 22:58:50 - Import completed
15.11.2017 22:58:51 - Validation ended with 0 error(s)
15.11.2017 22:58:51 - Compiling test script
15.11.2017 22:58:51 - Starting XQuery tests
15.11.2017 22:58:51 - "Testing 1 features"
15.11.2017 22:58:51 - "Indexing features (parsing errors: 0): 145 ms"
15.11.2017 22:58:51 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-as/ets-tn-as-bsxets.xml"
15.11.2017 22:58:51 - "Statistics table: 1 ms"
15.11.2017 22:58:51 - "Test Suite 'Conformance class: Application schema, Transport Networks Common' started"
15.11.2017 22:58:51 - "Test Case 'Code list values' started"
15.11.2017 22:58:51 - "Test Assertion 'tn-as.a.1: AccessRestrictionValue attributes': PASSED - 12 ms"
15.11.2017 22:58:51 - "Test Assertion 'tn-as.a.2: RestrictionTypeValue attributes': PASSED - 5 ms"
15.11.2017 22:58:51 - "Test Case 'Code list values' finished: PASSED"
15.11.2017 22:58:51 - "Test Case 'Constraints' started"
15.11.2017 22:58:51 - "Test Assertion 'tn-as.b.1: TrafficFlowDirection can only be associated with a spatial object of the type Link or LinkSequence.': PASSED - 0 ms"
15.11.2017 22:58:51 - "Test Assertion 'tn-as.b.2: All transport areas have an external object identifier.': PASSED - 0 ms"
15.11.2017 22:58:51 - "Test Assertion 'tn-as.b.3: All transport links have an external object identifier.': PASSED - 0 ms"
15.11.2017 22:58:51 - "Test Assertion 'tn-as.b.4: All transport link sequences have an external object identifier.': PASSED - 0 ms"
15.11.2017 22:58:51 - "Test Assertion 'tn-as.b.5: All transport link sets have an external object identifier.': PASSED - 0 ms"
15.11.2017 22:58:51 - "Test Assertion 'tn-as.b.6: All transport nodes have an external object identifier.': PASSED - 0 ms"
15.11.2017 22:58:51 - "Test Assertion 'tn-as.b.7: All transport points have an external object identifier.': PASSED - 1 ms"
15.11.2017 22:58:51 - "Test Assertion 'tn-as.b.8: All transport properties have an external object identifier.': PASSED - 0 ms"
15.11.2017 22:58:51 - "Test Assertion 'tn-as.b.9: A transport link sequence must be composed of transport links that all belong to the same transport network.': PASSED - 0 ms"
15.11.2017 22:58:51 - "Test Assertion 'tn-as.b.10: A transport link set must be composed of transport links and or transport link sequences that all belong to the same transport network.': PASSED - 0 ms"
15.11.2017 22:58:51 - "Test Case 'Constraints' finished: PASSED"
15.11.2017 22:58:51 - "Test Case 'Geometry' started"
15.11.2017 22:58:51 - "Test Assertion 'tn-as.c.1: No free transport nodes': PASSED - 0 ms"
15.11.2017 22:58:51 - "Test Assertion 'tn-as.c.2: Intersections only at crossings': PASSED_MANUAL"
15.11.2017 22:58:51 - "Test Case 'Geometry' finished: PASSED_MANUAL"
15.11.2017 22:58:51 - "Test Case 'Network connectivity' started"
15.11.2017 22:58:51 - "Test Assertion 'tn-as.d.1: Connectivity at crossings': PASSED_MANUAL"
15.11.2017 22:58:51 - "Test Assertion 'tn-as.d.2: Unconnected nodes': PASSED_MANUAL"
15.11.2017 22:58:51 - "Test Assertion 'tn-as.d.3: Connectivity tolerance': PASSED_MANUAL"
15.11.2017 22:58:51 - "Test Case 'Network connectivity' finished: PASSED_MANUAL"
15.11.2017 22:58:51 - "Test Case 'Object References' started"
15.11.2017 22:58:51 - "Test Assertion 'tn-as.e.1: Linear references': PASSED_MANUAL"
15.11.2017 22:58:51 - "Test Assertion 'tn-as.e.2: Inter-modal connections': PASSED - 0 ms"
15.11.2017 22:58:51 - "Test Case 'Object References' finished: PASSED_MANUAL"
15.11.2017 22:58:51 - "Test Suite 'Conformance class: Application schema, Transport Networks Common' finished: PASSED_MANUAL"
15.11.2017 22:58:52 - Releasing resources
15.11.2017 22:58:52 - TestRunTask initialized
15.11.2017 22:58:52 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 22:58:52 - Validation ended with 0 error(s)
15.11.2017 22:58:52 - Compiling test script
15.11.2017 22:58:52 - Starting XQuery tests
15.11.2017 22:58:52 - "Testing 1 features"
15.11.2017 22:58:52 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-ra-as/ets-tn-ra-as-bsxets.xml"
15.11.2017 22:58:52 - "Statistics table: 0 ms"
15.11.2017 22:58:52 - "Test Suite 'Conformance class: Application schema, Rail Transport Networks' started"
15.11.2017 22:58:52 - "Test Case 'Code list values' started"
15.11.2017 22:58:52 - "Test Assertion 'tn-ra-as.a.1: FormOfRailwayNodeValue attributes': PASSED - 5 ms"
15.11.2017 22:58:52 - "Test Assertion 'tn-ra-as.a.2: RailwayTypeValue attributes': PASSED - 4 ms"
15.11.2017 22:58:52 - "Test Assertion 'tn-ra-as.a.3: RailwayUseValue attributes': PASSED - 3 ms"
15.11.2017 22:58:52 - "Test Case 'Code list values' finished: PASSED"
15.11.2017 22:58:52 - "Test Case 'Constraints' started"
15.11.2017 22:58:52 - "Test Assertion 'tn-ra-as.b.1: DesignSpeed can only be associated with a spatial object that is part of a railway transport network.': PASSED - 1 ms"
15.11.2017 22:58:52 - "Test Assertion 'tn-ra-as.b.2: NominalTrackGauge can only be associated with a spatial object that is part of a railway transport network.': PASSED - 0 ms"
15.11.2017 22:58:52 - "Test Assertion 'tn-ra-as.b.3: NumberOfTracks can only be associated with a spatial object that is part of a railway transport network.': PASSED - 0 ms"
15.11.2017 22:58:52 - "Test Assertion 'tn-ra-as.b.4: RailwayElectrification can only be associated with a spatial object that is part of a railway transport network.': PASSED - 0 ms"
15.11.2017 22:58:52 - "Test Assertion 'tn-ra-as.b.5: RailwayStationCode can only be associated with a spatial object that is part of a railway transport network.': PASSED - 0 ms"
15.11.2017 22:58:52 - "Test Assertion 'tn-ra-as.b.6: RailwayType can only be associated with a spatial object that is part of a railway transport network.': PASSED - 0 ms"
15.11.2017 22:58:52 - "Test Assertion 'tn-ra-as.b.7: RailwayUse can only be associated with a spatial object that is part of a railway transport network.': PASSED - 0 ms"
15.11.2017 22:58:52 - "Test Assertion 'tn-ra-as.b.8: For a railway station node, the value for the ""formOfNode"" attribute shall always be ""RailwayStop"".': PASSED - 0 ms"
15.11.2017 22:58:52 - "Test Assertion 'tn-ra-as.b.9: For a railway yard node, the value for the ""formOfNode"" attribute shall always be ""RailwayStop"".': PASSED - 0 ms"
15.11.2017 22:58:52 - "Test Case 'Constraints' finished: PASSED"
15.11.2017 22:58:52 - "Test Case 'Link centrelines' started"
15.11.2017 22:58:52 - "Test Assertion 'tn-ro-as.a.1: Link centrelines test': PASSED_MANUAL"
15.11.2017 22:58:52 - "Test Case 'Link centrelines' finished: PASSED_MANUAL"
15.11.2017 22:58:52 - "Test Suite 'Conformance class: Application schema, Rail Transport Networks' finished: PASSED_MANUAL"
15.11.2017 22:58:52 - Releasing resources
15.11.2017 22:58:52 - TestRunTask initialized
15.11.2017 22:58:52 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 22:58:52 - Validation ended with 0 error(s)
15.11.2017 22:58:52 - Compiling test script
15.11.2017 22:58:52 - Starting XQuery tests
15.11.2017 22:58:52 - "Testing 1 features"
15.11.2017 22:58:52 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-encoding/inspire-gml/ets-inspire-gml-bsxets.xml"
15.11.2017 22:58:52 - "Statistics table: 0 ms"
15.11.2017 22:58:52 - "Test Suite 'Conformance class: INSPIRE GML encoding' started"
15.11.2017 22:58:52 - "Test Case 'Basic tests' started"
15.11.2017 22:58:52 - "Test Assertion 'gml.a.1: Errors loading the XML documents': PASSED - 0 ms"
15.11.2017 22:58:52 - "Test Assertion 'gml.a.2: Document root element': PASSED - 0 ms"
15.11.2017 22:58:52 - "Test Assertion 'gml.a.3: Character encoding': NOT_APPLICABLE"
15.11.2017 22:58:52 - "Test Case 'Basic tests' finished: PASSED"
15.11.2017 22:58:52 - "Test Suite 'Conformance class: INSPIRE GML encoding' finished: PASSED"
15.11.2017 22:58:53 - Releasing resources
15.11.2017 22:58:53 - TestRunTask initialized
15.11.2017 22:58:53 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 22:58:53 - Validation ended with 0 error(s)
15.11.2017 22:58:53 - Compiling test script
15.11.2017 22:58:53 - Starting XQuery tests
15.11.2017 22:58:53 - "Testing 1 features"
15.11.2017 22:58:53 - "Indexing features (parsing errors: 0): 139 ms"
15.11.2017 22:58:53 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/data-consistency/ets-data-consistency-bsxets.xml"
15.11.2017 22:58:53 - "Statistics table: 1 ms"
15.11.2017 22:58:53 - "Test Suite 'Conformance class: Data consistency, General requirements' started"
15.11.2017 22:58:53 - "Test Case 'Version consistency' started"
15.11.2017 22:58:53 - "Test Assertion 'dc.a.1: Version lifespan plausible': PASSED - 0 ms"
15.11.2017 22:58:53 - "Test Assertion 'dc.a.2: Unique identifier persistency': PASSED_MANUAL"
15.11.2017 22:58:53 - "Test Assertion 'dc.a.3: Spatial object type stable': PASSED_MANUAL"
15.11.2017 22:58:53 - "Test Case 'Version consistency' finished: PASSED_MANUAL"
15.11.2017 22:58:53 - "Test Case 'Temporal consistency' started"
15.11.2017 22:58:53 - "Test Assertion 'dc.b.1: Valid time plausible': PASSED - 0 ms"
15.11.2017 22:58:53 - "Test Case 'Temporal consistency' finished: PASSED"
15.11.2017 22:58:53 - "Test Suite 'Conformance class: Data consistency, General requirements' finished: PASSED_MANUAL"
15.11.2017 22:58:53 - Releasing resources
15.11.2017 22:58:53 - TestRunTask initialized
15.11.2017 22:58:53 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 22:58:53 - Validation ended with 0 error(s)
15.11.2017 22:58:53 - Compiling test script
15.11.2017 22:58:53 - Starting XQuery tests
15.11.2017 22:58:54 - "Testing 1 features"
15.11.2017 22:58:54 - "Indexing features (parsing errors: 0): 139 ms"
15.11.2017 22:58:54 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-dc/ets-tn-dc-bsxets.xml"
15.11.2017 22:58:54 - "Statistics table: 1 ms"
15.11.2017 22:58:54 - "Test Suite 'Conformance class: Data consistency, Transport Networks' started"
15.11.2017 22:58:54 - "Test Case 'Spatial consistency' started"
15.11.2017 22:58:54 - "Test Assertion 'tn-dc.a.1: Each Transport Network Link or Node geometry is within a Network Area geometry (Road Transport Networks)': PASSED - 0 ms"
15.11.2017 22:58:54 - "Test Assertion 'tn-dc.a.2: Each Transport Network Link or Node geometry is within a Network Area geometry (Railway Transport Networks)': PASSED - 0 ms"
15.11.2017 22:58:54 - "Test Assertion 'tn-dc.a.3: Each Transport Network Link or Node geometry is within a Network Area geometry (Waterway Transport Networks)': PASSED - 0 ms"
15.11.2017 22:58:54 - "Test Assertion 'tn-dc.a.4: Each Transport Network Link or Node geometry is within a Network Area geometry (Air Transport Networks)': PASSED - 0 ms"
15.11.2017 22:58:54 - "Test Assertion 'tn-dc.a.5: Manual review': PASSED_MANUAL"
15.11.2017 22:58:54 - "Test Case 'Spatial consistency' finished: PASSED_MANUAL"
15.11.2017 22:58:54 - "Test Suite 'Conformance class: Data consistency, Transport Networks' finished: PASSED_MANUAL"
15.11.2017 22:58:54 - Releasing resources
15.11.2017 22:58:54 - TestRunTask initialized
15.11.2017 22:58:54 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 22:58:54 - Validation ended with 0 error(s)
15.11.2017 22:58:54 - Compiling test script
15.11.2017 22:58:54 - Starting XQuery tests
15.11.2017 22:58:54 - "Testing 1 features"
15.11.2017 22:58:54 - "Indexing features (parsing errors: 0): 140 ms"
15.11.2017 22:58:54 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/information-accessibility/ets-information-accessibility-bsxets.xml"
15.11.2017 22:58:54 - "Statistics table: 0 ms"
15.11.2017 22:58:54 - "Test Suite 'Conformance class: Information accessibility, General requirements' started"
15.11.2017 22:58:54 - "Test Case 'Coordinate reference systems (CRS)' started"
15.11.2017 22:58:54 - "Test Assertion 'ia.a.1: CRS publicly accessible via HTTP': FAILED - 1 ms"
15.11.2017 22:58:54 - "Test Case 'Coordinate reference systems (CRS)' finished: FAILED"
15.11.2017 22:58:54 - "Test Suite 'Conformance class: Information accessibility, General requirements' finished: FAILED"
15.11.2017 22:58:55 - Releasing resources
15.11.2017 22:58:55 - TestRunTask initialized
15.11.2017 22:58:55 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 22:58:55 - Validation ended with 0 error(s)
15.11.2017 22:58:55 - Compiling test script
15.11.2017 22:58:55 - Starting XQuery tests
15.11.2017 22:58:55 - "Testing 1 features"
15.11.2017 22:58:55 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-ia/ets-tn-ia-bsxets.xml"
15.11.2017 22:58:55 - "Statistics table: 1 ms"
15.11.2017 22:58:55 - "Test Suite 'Conformance class: Information accessibility, Transport Networks' started"
15.11.2017 22:58:55 - "Test Case 'Code lists' started"
15.11.2017 22:58:55 - "Test Assertion 'tn-ia.a.1: Code list extensions accessible': PASSED - 0 ms"
15.11.2017 22:58:55 - "Test Case 'Code lists' finished: PASSED"
15.11.2017 22:58:55 - "Test Case 'Feature references' started"
15.11.2017 22:58:55 - "Test Assertion 'tn-ia.b.1: MarkerPost.route': PASSED - 0 ms"
15.11.2017 22:58:55 - "Test Assertion 'tn-ia.b.2: TransportLinkSet.post': PASSED - 0 ms"
15.11.2017 22:58:55 - "Test Case 'Feature references' finished: PASSED"
15.11.2017 22:58:55 - "Test Suite 'Conformance class: Information accessibility, Transport Networks' finished: PASSED"
15.11.2017 22:58:56 - Releasing resources
15.11.2017 22:58:56 - TestRunTask initialized
15.11.2017 22:58:56 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 22:58:56 - Validation ended with 0 error(s)
15.11.2017 22:58:56 - Compiling test script
15.11.2017 22:58:56 - Starting XQuery tests
15.11.2017 22:58:56 - "Testing 1 features"
15.11.2017 22:58:56 - "Indexing features (parsing errors: 0): 143 ms"
15.11.2017 22:58:56 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/reference-systems/ets-reference-systems-bsxets.xml"
15.11.2017 22:58:56 - "Statistics table: 1 ms"
15.11.2017 22:58:56 - "Test Suite 'Conformance class: Reference systems, General requirements' started"
15.11.2017 22:58:56 - "Test Case 'Spatial reference systems' started"
15.11.2017 22:58:56 - "Test Assertion 'rs.a.1: Spatial reference systems in feature geometries': FAILED - 0 ms"
15.11.2017 22:58:56 - "Test Assertion 'rs.a.2: Default spatial reference systems in feature collections': PASSED - 0 ms"
15.11.2017 22:58:56 - "Test Case 'Spatial reference systems' finished: FAILED"
15.11.2017 22:58:56 - "Test Case 'Temporal reference systems' started"
15.11.2017 22:58:56 - "Test Assertion 'rs.a.3: Temporal reference systems in features': PASSED - 0 ms"
15.11.2017 22:58:56 - "Test Case 'Temporal reference systems' finished: PASSED"
15.11.2017 22:58:56 - "Test Suite 'Conformance class: Reference systems, General requirements' finished: FAILED"
15.11.2017 22:58:57 - Releasing resources
15.11.2017 22:58:57 - TestRunTask initialized
15.11.2017 22:58:57 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 22:58:57 - Validation ended with 0 error(s)
15.11.2017 22:58:57 - Compiling test script
15.11.2017 22:58:57 - Starting XQuery tests
15.11.2017 22:58:57 - "Testing 1 features"
15.11.2017 22:58:57 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-rs/ets-tn-rs-bsxets.xml"
15.11.2017 22:58:57 - "Statistics table: 0 ms"
15.11.2017 22:58:57 - "Test Suite 'Conformance class: Reference systems, Transport Networks' started"
15.11.2017 22:58:57 - "Test Case 'Additional theme-specific rules for reference systems' started"
15.11.2017 22:58:57 - "Test Assertion 'tn-rs.a.1: Test always passes': PASSED - 0 ms"
15.11.2017 22:58:57 - "Test Case 'Additional theme-specific rules for reference systems' finished: PASSED"
15.11.2017 22:58:57 - "Test Suite 'Conformance class: Reference systems, Transport Networks' finished: PASSED"
15.11.2017 22:58:57 - Releasing resources
15.11.2017 22:58:57 - Changed state from INITIALIZED to RUNNING
15.11.2017 22:58:57 - Duration: 9sec
15.11.2017 22:58:57 - TestRun finished
15.11.2017 22:58:57 - Changed state from RUNNING to COMPLETED
