12.11.2017 18:55:46 - Preparing Test Run metadata (initiated Sun Nov 12 18:55:46 CET 2017)
12.11.2017 18:55:46 - Resolving Executable Test Suite dependencies
12.11.2017 18:55:46 - Preparing 2 Test Task:
12.11.2017 18:55:46 -  TestTask 1 (7f3c989f-a475-4a31-abd5-412b83589093)
12.11.2017 18:55:46 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
12.11.2017 18:55:46 -  with parameters: 
12.11.2017 18:55:46 - etf.testcases = *
12.11.2017 18:55:46 -  TestTask 2 (d5e56b58-55e7-4d79-baf2-3beb6db7aa3e)
12.11.2017 18:55:46 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.4 )'
12.11.2017 18:55:46 -  with parameters: 
12.11.2017 18:55:46 - etf.testcases = *
12.11.2017 18:55:46 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
12.11.2017 18:55:46 - Setting state to CREATED
12.11.2017 18:55:46 - Changed state from CREATED to INITIALIZING
12.11.2017 18:55:46 - Starting TestRun.5be89df4-c8ec-4f54-8b4d-f8975fa730c8 at 2017-11-12T18:55:47+01:00
12.11.2017 18:55:47 - Changed state from INITIALIZING to INITIALIZED
12.11.2017 18:55:47 - TestRunTask initialized
12.11.2017 18:55:47 - Creating new tests databases to speed up tests.
12.11.2017 18:55:47 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.11.2017 18:55:47 - Optimizing last database etf-tdb-613412fd-d9e6-4931-a67d-74edf175643f-0 
12.11.2017 18:55:47 - Import completed
12.11.2017 18:55:48 - Validation ended with 0 error(s)
12.11.2017 18:55:48 - Compiling test script
12.11.2017 18:55:48 - Starting XQuery tests
12.11.2017 18:55:48 - "Testing 1 records"
12.11.2017 18:55:48 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
12.11.2017 18:55:48 - "Statistics table: 1 ms"
12.11.2017 18:55:48 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
12.11.2017 18:55:48 - "Test Case 'Schema validation' started"
12.11.2017 18:55:50 - "Validating file GetRecordByIdResponse.xml: 2236 ms"
12.11.2017 18:55:50 - "Test Assertion 'md-xml.a.1: Validate XML documents': FAILED - 2237 ms"
12.11.2017 18:55:50 - "Test Case 'Schema validation' finished: FAILED"
12.11.2017 18:55:50 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: FAILED"
12.11.2017 18:55:50 - Releasing resources
12.11.2017 18:55:50 - TestRunTask initialized
12.11.2017 18:55:50 - Recreating new tests databases as the Test Object has changed!
12.11.2017 18:55:50 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.11.2017 18:55:50 - Optimizing last database etf-tdb-613412fd-d9e6-4931-a67d-74edf175643f-0 
12.11.2017 18:55:51 - Import completed
12.11.2017 18:55:51 - Validation ended with 0 error(s)
12.11.2017 18:55:51 - Compiling test script
12.11.2017 18:55:51 - Starting XQuery tests
12.11.2017 18:55:51 - "Testing 1 records"
12.11.2017 18:55:51 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
12.11.2017 18:55:51 - "Statistics table: 1 ms"
12.11.2017 18:55:51 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
12.11.2017 18:55:51 - "Test Case 'Common tests' started"
12.11.2017 18:55:51 - "Test Assertion 'md-iso.a.1: Title': PASSED - 1 ms"
12.11.2017 18:55:51 - "Test Assertion 'md-iso.a.2: Abstract': FAILED - 0 ms"
12.11.2017 18:55:51 - "Test Assertion 'md-iso.a.3: Access and use conditions': FAILED - 0 ms"
12.11.2017 18:55:51 - "Test Assertion 'md-iso.a.4: Public access': FAILED - 0 ms"
12.11.2017 18:55:51 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 0 ms"
12.11.2017 18:55:51 - "Test Assertion 'md-iso.a.6: Language': FAILED - 0 ms"
12.11.2017 18:55:51 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 1 ms"
12.11.2017 18:55:51 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
12.11.2017 18:55:51 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 0 ms"
12.11.2017 18:55:51 - "Test Assertion 'md-iso.a.10: Responsible party contact info': FAILED - 0 ms"
12.11.2017 18:55:51 - "Test Assertion 'md-iso.a.11: Responsible party role': FAILED - 1 ms"
12.11.2017 18:55:51 - "Test Case 'Common tests' finished: FAILED"
12.11.2017 18:55:51 - "Test Case 'Hierarchy level' started"
12.11.2017 18:55:51 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 0 ms"
12.11.2017 18:55:51 - "Test Case 'Hierarchy level' finished: PASSED"
12.11.2017 18:55:51 - "Test Case 'Dataset (series) tests' started"
12.11.2017 18:55:51 - "Test Assertion 'md-iso.c.1: Dataset identification': FAILED - 0 ms"
12.11.2017 18:55:51 - "Test Assertion 'md-iso.c.2: Dataset language': FAILED - 0 ms"
12.11.2017 18:55:51 - "Checking URL: 'https://test-zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get'"
12.11.2017 18:55:51 - "Checking URL: 'https://test-zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?typename=au%3AAdministrativeUnit&amp;version=1.1.0&amp;request=GetFeature&amp;service=WFS'"
12.11.2017 18:55:57 - "Test Assertion 'md-iso.c.3: Dataset linkage': PASSED_MANUAL - 6506 ms"
12.11.2017 18:55:57 - "Test Assertion 'md-iso.c.4: Dataset conformity': FAILED - 0 ms"
12.11.2017 18:55:57 - "Test Assertion 'md-iso.c.5: Dataset topic': FAILED - 1 ms"
12.11.2017 18:55:57 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': FAILED - 0 ms"
12.11.2017 18:55:57 - "Test Assertion 'md-iso.c.7: Dataset lineage': FAILED - 0 ms"
12.11.2017 18:55:57 - "Test Case 'Dataset (series) tests' finished: FAILED"
12.11.2017 18:55:57 - "Test Case 'Service tests' started"
12.11.2017 18:55:57 - "Test Assertion 'md-iso.d.1: Service type': PASSED - 0 ms"
12.11.2017 18:55:57 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED - 0 ms"
12.11.2017 18:55:57 - "Test Assertion 'md-iso.d.3: Coupled resource': PASSED - 0 ms"
12.11.2017 18:55:57 - "Test Case 'Service tests' finished: PASSED"
12.11.2017 18:55:57 - "Test Case 'Keywords' started"
12.11.2017 18:55:57 - "Test Assertion 'md-iso.e.1: Keywords': FAILED - 1 ms"
12.11.2017 18:55:57 - "Test Case 'Keywords' finished: FAILED"
12.11.2017 18:55:57 - "Test Case 'Keywords - details' started"
12.11.2017 18:55:57 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 0 ms"
12.11.2017 18:55:57 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 0 ms"
12.11.2017 18:55:57 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 0 ms"
12.11.2017 18:55:57 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 0 ms"
12.11.2017 18:55:57 - "Test Case 'Keywords - details' finished: PASSED"
12.11.2017 18:55:57 - "Test Case 'Temporal extent' started"
12.11.2017 18:55:57 - "Test Assertion 'md-iso.g.1: Temporal extent': FAILED - 0 ms"
12.11.2017 18:55:57 - "Test Case 'Temporal extent' finished: FAILED"
12.11.2017 18:55:57 - "Test Case 'Temporal extent - details' started"
12.11.2017 18:55:57 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 0 ms"
12.11.2017 18:55:57 - "Test Case 'Temporal extent - details' finished: PASSED"
12.11.2017 18:55:57 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: FAILED"
12.11.2017 18:55:58 - Releasing resources
12.11.2017 18:55:58 - Changed state from INITIALIZED to RUNNING
12.11.2017 18:55:59 - Duration: 12sec
12.11.2017 18:55:59 - TestRun finished
12.11.2017 18:55:59 - Changed state from RUNNING to COMPLETED
