31.01.2018 22:57:13 - Preparing Test Run interoperability_17316219_Geodetick&yacute;akartografick&yacute;&uacute;stavBratislava_tn-ro:RoadWidth (initiated Wed Jan 31 22:57:13 CET 2018)
31.01.2018 22:57:13 - Resolving Executable Test Suite dependencies
31.01.2018 22:57:13 - Preparing 11 Test Task:
31.01.2018 22:57:13 -  TestTask 1 (2a8ca70c-1438-458f-b463-e399e28929b0)
31.01.2018 22:57:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML encoding (EID: 545f9e49-009b-4114-9333-7ca26413b5d4, V: 0.2.1 )'
31.01.2018 22:57:13 -  with parameters: 
31.01.2018 22:57:13 - etf.testcases = *
31.01.2018 22:57:13 -  TestTask 2 (366e9f99-873b-4f7a-9f40-9d30d51b2bde)
31.01.2018 22:57:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements (EID: 09820daf-62b2-4fa3-a95f-56a0d2b7c4d8, V: 0.2.4 )'
31.01.2018 22:57:13 -  with parameters: 
31.01.2018 22:57:13 - etf.testcases = *
31.01.2018 22:57:13 -  TestTask 3 (9c3bd15c-379c-4f81-b3a5-8f9629187263)
31.01.2018 22:57:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.9af1c865-1cf0-43ff-9250-069df01b0948'
31.01.2018 22:57:13 -  with parameters: 
31.01.2018 22:57:13 - etf.testcases = *
31.01.2018 22:57:13 -  TestTask 4 (fa9f46ef-4f26-4b71-882c-bcc7e78e4635)
31.01.2018 22:57:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.4441cbde-371f-4899-90b3-145f4fd08ebc'
31.01.2018 22:57:13 -  with parameters: 
31.01.2018 22:57:13 - etf.testcases = *
31.01.2018 22:57:13 -  TestTask 5 (4d3b28b1-ea95-4394-b56d-7ad9ea4e1f25)
31.01.2018 22:57:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Application schema, Road Transport Networks (EID: 14986e54-74c4-43b0-979b-d0d3e5cd0e8c, V: 0.2.1 )'
31.01.2018 22:57:13 -  with parameters: 
31.01.2018 22:57:13 - etf.testcases = *
31.01.2018 22:57:13 -  TestTask 6 (d3d95c67-0dd8-4654-aac2-6bc5ce731d0c)
31.01.2018 22:57:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
31.01.2018 22:57:13 -  with parameters: 
31.01.2018 22:57:13 - etf.testcases = *
31.01.2018 22:57:13 -  TestTask 7 (216813bc-2edb-4270-8e59-831f476e5783)
31.01.2018 22:57:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Data consistency, Transport Networks (EID: 733af9a0-312b-4f71-9aa2-977cd2134d23, V: 0.2.2 )'
31.01.2018 22:57:13 -  with parameters: 
31.01.2018 22:57:13 - etf.testcases = *
31.01.2018 22:57:13 -  TestTask 8 (7efdce8a-a9af-44c4-a179-b4adf3a04c37)
31.01.2018 22:57:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
31.01.2018 22:57:13 -  with parameters: 
31.01.2018 22:57:13 - etf.testcases = *
31.01.2018 22:57:13 -  TestTask 9 (11104f5f-c916-4014-8b48-61ce7f5a504b)
31.01.2018 22:57:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Transport Networks (EID: df5db9a4-b15f-4193-a6ff-6e9951af46f5, V: 0.2.2 )'
31.01.2018 22:57:13 -  with parameters: 
31.01.2018 22:57:13 - etf.testcases = *
31.01.2018 22:57:13 -  TestTask 10 (938abdeb-f4e8-4601-8a46-1ef9935637b8)
31.01.2018 22:57:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
31.01.2018 22:57:13 -  with parameters: 
31.01.2018 22:57:13 - etf.testcases = *
31.01.2018 22:57:13 -  TestTask 11 (f1adec91-e9f5-4e7b-a881-68edb8bbddb4)
31.01.2018 22:57:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Reference systems, Transport Networks (EID: 9d35024d-9dd7-43a9-afff-d5aea5f51595, V: 0.2.0 )'
31.01.2018 22:57:13 -  with parameters: 
31.01.2018 22:57:13 - etf.testcases = *
31.01.2018 22:57:13 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
31.01.2018 22:57:13 - Setting state to CREATED
31.01.2018 22:57:13 - Changed state from CREATED to INITIALIZING
