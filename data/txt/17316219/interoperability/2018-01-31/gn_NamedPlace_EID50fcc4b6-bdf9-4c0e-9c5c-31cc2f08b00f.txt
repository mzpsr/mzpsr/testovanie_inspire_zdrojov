31.01.2018 22:55:31 - Preparing Test Run interoperability_17316219_Geodetick&yacute;akartografick&yacute;&uacute;stavBratislava(GK&Uacute;)_gn:NamedPlace (initiated Wed Jan 31 22:55:31 CET 2018)
31.01.2018 22:55:31 - Resolving Executable Test Suite dependencies
31.01.2018 22:55:31 - Preparing 10 Test Task:
31.01.2018 22:55:31 -  TestTask 1 (c015d055-bfa4-40c8-b9b1-596d7931cfec)
31.01.2018 22:55:31 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML encoding (EID: 545f9e49-009b-4114-9333-7ca26413b5d4, V: 0.2.1 )'
31.01.2018 22:55:31 -  with parameters: 
31.01.2018 22:55:31 - etf.testcases = *
31.01.2018 22:55:31 -  TestTask 2 (5498f715-5ba1-49b7-9472-9a1115a0c3dd)
31.01.2018 22:55:31 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements (EID: 09820daf-62b2-4fa3-a95f-56a0d2b7c4d8, V: 0.2.4 )'
31.01.2018 22:55:31 -  with parameters: 
31.01.2018 22:55:31 - etf.testcases = *
31.01.2018 22:55:31 -  TestTask 3 (4293266b-0e49-483d-bf8a-91ffe406b807)
31.01.2018 22:55:31 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.02b7b0cb-429a-4f4e-b0db-988464fb9496'
31.01.2018 22:55:31 -  with parameters: 
31.01.2018 22:55:31 - etf.testcases = *
31.01.2018 22:55:31 -  TestTask 4 (82ef24bf-e908-48ef-bf72-62d419ade588)
31.01.2018 22:55:31 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Application schema, Geographical Names (EID: 0fc46305-c623-422b-b7d7-251c3b86eb7f, V: 0.2.1 )'
31.01.2018 22:55:31 -  with parameters: 
31.01.2018 22:55:31 - etf.testcases = *
31.01.2018 22:55:31 -  TestTask 5 (5b861342-9482-4c7f-9577-ae70a3982a2d)
31.01.2018 22:55:31 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
31.01.2018 22:55:31 -  with parameters: 
31.01.2018 22:55:31 - etf.testcases = *
31.01.2018 22:55:31 -  TestTask 6 (1f3448a9-4ed4-4c2e-8d5f-ca781005dbfe)
31.01.2018 22:55:31 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Data consistency, Geographical Names (EID: a32f76c7-f1d3-4d70-83ef-d51d2545fa2e, V: 0.2.0 )'
31.01.2018 22:55:31 -  with parameters: 
31.01.2018 22:55:31 - etf.testcases = *
31.01.2018 22:55:31 -  TestTask 7 (5fae6eef-c253-4103-a223-7c7b7807c0de)
31.01.2018 22:55:31 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
31.01.2018 22:55:31 -  with parameters: 
31.01.2018 22:55:31 - etf.testcases = *
31.01.2018 22:55:31 -  TestTask 8 (82ae4e22-6785-49fe-94d9-2d92e1445b1c)
31.01.2018 22:55:31 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Geographical Names (EID: c3379b85-853e-4a35-8c3d-b64191d94587, V: 0.2.1 )'
31.01.2018 22:55:31 -  with parameters: 
31.01.2018 22:55:31 - etf.testcases = *
31.01.2018 22:55:31 -  TestTask 9 (3d97c571-4bf8-4d99-badd-a8d692ebce2d)
31.01.2018 22:55:31 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
31.01.2018 22:55:31 -  with parameters: 
31.01.2018 22:55:31 - etf.testcases = *
31.01.2018 22:55:31 -  TestTask 10 (63fc747a-bec3-467a-80e9-85ea09ca92b2)
31.01.2018 22:55:31 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Reference systems, Geographical Names (EID: 1620bd27-b881-48a2-bf2b-301541e035f4, V: 0.2.0 )'
31.01.2018 22:55:31 -  with parameters: 
31.01.2018 22:55:31 - etf.testcases = *
31.01.2018 22:55:31 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
31.01.2018 22:55:31 - Setting state to CREATED
31.01.2018 22:55:31 - Changed state from CREATED to INITIALIZING
31.01.2018 22:55:52 - Starting TestRun.50fcc4b6-bdf9-4c0e-9c5c-31cc2f08b00f at 2018-01-31T22:55:54+01:00
31.01.2018 22:55:54 - Changed state from INITIALIZING to INITIALIZED
31.01.2018 22:55:54 - TestRunTask initialized
31.01.2018 22:55:54 - Creating new tests databases to speed up tests.
31.01.2018 22:55:54 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:55:54 - Optimizing last database etf-tdb-5c39ffe1-ebbf-4f95-9b0b-06f83c064be1-0 
31.01.2018 22:55:54 - Import completed
31.01.2018 22:55:54 - Validation ended with 0 error(s)
31.01.2018 22:55:54 - Compiling test script
31.01.2018 22:55:54 - Starting XQuery tests
31.01.2018 22:55:54 - "Testing 1 features"
31.01.2018 22:55:54 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-encoding/inspire-gml/ets-inspire-gml-bsxets.xml"
31.01.2018 22:55:54 - "Statistics table: 0 ms"
31.01.2018 22:55:54 - "Test Suite 'Conformance class: INSPIRE GML encoding' started"
31.01.2018 22:55:54 - "Test Case 'Basic tests' started"
31.01.2018 22:55:54 - "Test Assertion 'gml.a.1: Errors loading the XML documents': PASSED - 0 ms"
31.01.2018 22:55:54 - "Test Assertion 'gml.a.2: Document root element': PASSED - 0 ms"
31.01.2018 22:55:54 - "Test Assertion 'gml.a.3: Character encoding': NOT_APPLICABLE"
31.01.2018 22:55:54 - "Test Case 'Basic tests' finished: PASSED"
31.01.2018 22:55:54 - "Test Suite 'Conformance class: INSPIRE GML encoding' finished: PASSED"
31.01.2018 22:55:54 - Releasing resources
31.01.2018 22:55:54 - TestRunTask initialized
31.01.2018 22:55:54 - Recreating new tests databases as the Test Object has changed!
31.01.2018 22:55:54 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:55:54 - Optimizing last database etf-tdb-5c39ffe1-ebbf-4f95-9b0b-06f83c064be1-0 
31.01.2018 22:55:54 - Import completed
31.01.2018 22:55:54 - Validation ended with 0 error(s)
31.01.2018 22:55:54 - Compiling test script
31.01.2018 22:55:54 - Starting XQuery tests
31.01.2018 22:55:55 - "Testing 1 features"
31.01.2018 22:55:55 - "Indexing features (parsing errors: 0): 40 ms"
31.01.2018 22:55:55 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/schemas/ets-schemas-bsxets.xml"
31.01.2018 22:55:55 - "Statistics table: 1 ms"
31.01.2018 22:55:55 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' started"
31.01.2018 22:55:55 - "Test Case 'Schema' started"
31.01.2018 22:55:55 - "Test Assertion 'gmlas.a.1: Mapping of source data to INSPIRE': PASSED_MANUAL"
31.01.2018 22:55:55 - "Test Assertion 'gmlas.a.2: Modelling of additional spatial object types': PASSED_MANUAL"
31.01.2018 22:55:55 - "Test Case 'Schema' finished: PASSED_MANUAL"
31.01.2018 22:55:55 - "Test Case 'Schema validation' started"
31.01.2018 22:55:55 - "Test Assertion 'gmlas.b.1: xsi:schemaLocation attribute': PASSED - 0 ms"
31.01.2018 22:55:55 - "Validating get.xml"
31.01.2018 22:56:03 - "Duration: 8289 ms. Errors: 1."
31.01.2018 22:56:03 - "Test Assertion 'gmlas.b.2: validate XML documents': FAILED - 8289 ms"
31.01.2018 22:56:03 - "Test Case 'Schema validation' finished: FAILED"
31.01.2018 22:56:03 - "Test Case 'GML model' started"
31.01.2018 22:56:03 - "Test Assertion 'gmlas.c.1: Consistency with the GML model': PASSED - 0 ms"
31.01.2018 22:56:03 - "Test Assertion 'gmlas.c.2: nilReason attributes require xsi:nil=true': PASSED - 0 ms"
31.01.2018 22:56:03 - "Test Assertion 'gmlas.c.3: nilReason values': PASSED - 0 ms"
31.01.2018 22:56:03 - "Test Case 'GML model' finished: PASSED"
31.01.2018 22:56:03 - "Test Case 'Simple features' started"
31.01.2018 22:56:03 - "Test Assertion 'gmlas.d.1: No spatial topology objects': PASSED - 0 ms"
31.01.2018 22:56:03 - "Test Assertion 'gmlas.d.2: No non-linear interpolation': PASSED - 0 ms"
31.01.2018 22:56:03 - "Test Assertion 'gmlas.d.3: Surface geometry elements': PASSED - 0 ms"
31.01.2018 22:56:03 - "Test Assertion 'gmlas.d.4: No non-planar interpolation': PASSED - 1 ms"
31.01.2018 22:56:03 - "Test Assertion 'gmlas.d.5: Geometry elements': PASSED - 0 ms"
31.01.2018 22:56:03 - "Test Assertion 'gmlas.d.6: Point coordinates in gml:pos': PASSED - 0 ms"
31.01.2018 22:56:03 - "Test Assertion 'gmlas.d.7: Curve/Surface coordinates in gml:posList': PASSED - 0 ms"
31.01.2018 22:56:03 - "Test Assertion 'gmlas.d.8: No array property elements': PASSED - 0 ms"
31.01.2018 22:56:03 - "Test Assertion 'gmlas.d.9: 1, 2 or 3 coordinate dimensions': PASSED - 0 ms"
31.01.2018 22:56:03 - "Test Assertion 'gmlas.d.10: Validate geometries (1)': PASSED - 27 ms"
31.01.2018 22:56:03 - "Test Assertion 'gmlas.d.11: Validate geometries (2)': PASSED - 0 ms"
31.01.2018 22:56:03 - "Test Case 'Simple features' finished: PASSED"
31.01.2018 22:56:03 - "Test Case 'Code list values in basic data types' started"
31.01.2018 22:56:03 - "Test Assertion 'gmlas.e.1: GrammaticalNumber attributes': PASSED - 7 ms"
31.01.2018 22:56:03 - "Test Assertion 'gmlas.e.2: GrammaticalGender attributes': PASSED - 3 ms"
31.01.2018 22:56:03 - "Test Assertion 'gmlas.e.3: NameStatus attributes': PASSED - 4 ms"
31.01.2018 22:56:03 - "Test Assertion 'gmlas.e.4: Nativeness attributes': PASSED - 3 ms"
31.01.2018 22:56:03 - "Test Case 'Code list values in basic data types' finished: PASSED"
31.01.2018 22:56:03 - "Test Case 'Constraints' started"
31.01.2018 22:56:03 - "Test Assertion 'gmlas.f.1: At least one of the two attributes pronunciationSoundLink and pronunciationIPA shall not be void': PASSED - 0 ms"
31.01.2018 22:56:03 - "Test Case 'Constraints' finished: PASSED"
31.01.2018 22:56:03 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' finished: FAILED"
31.01.2018 22:56:07 - Releasing resources
31.01.2018 22:56:07 - TestRunTask initialized
31.01.2018 22:56:07 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:56:07 - Validation ended with 0 error(s)
31.01.2018 22:56:07 - Compiling test script
31.01.2018 22:56:07 - Starting XQuery tests
31.01.2018 22:56:07 - "Testing 1 features"
31.01.2018 22:56:07 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-gn/gn-gml/ets-gn-gml-bsxets.xml"
31.01.2018 22:56:07 - "Statistics table: 0 ms"
31.01.2018 22:56:07 - "Test Suite 'Conformance class: GML application schemas, Geographical Names' started"
31.01.2018 22:56:07 - "Test Case 'Basic test' started"
31.01.2018 22:56:07 - "Test Assertion 'gn-gml.a.1: Geographical Names feature in dataset': PASSED - 0 ms"
31.01.2018 22:56:07 - "Test Case 'Basic test' finished: PASSED"
31.01.2018 22:56:07 - "Test Suite 'Conformance class: GML application schemas, Geographical Names' finished: PASSED"
31.01.2018 22:56:07 - Releasing resources
31.01.2018 22:56:07 - TestRunTask initialized
31.01.2018 22:56:07 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:56:07 - Validation ended with 0 error(s)
31.01.2018 22:56:07 - Compiling test script
31.01.2018 22:56:07 - Starting XQuery tests
31.01.2018 22:56:07 - "Testing 1 features"
31.01.2018 22:56:07 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-gn/gn-as/ets-gn-as-bsxets.xml"
31.01.2018 22:56:07 - "Statistics table: 0 ms"
31.01.2018 22:56:07 - "Test Suite 'Conformance class: Application schema, Geographical Names' started"
31.01.2018 22:56:07 - "Test Case 'Code list values' started"
31.01.2018 22:56:07 - "Test Assertion 'gn-as.a.1: NamedPlaceType attributes': PASSED - 8 ms"
31.01.2018 22:56:07 - "Test Case 'Code list values' finished: PASSED"
31.01.2018 22:56:07 - "Test Case 'Constraints' started"
31.01.2018 22:56:07 - "Test Assertion 'gn-as.b.1: Test always passes': PASSED - 0 ms"
31.01.2018 22:56:07 - "Test Case 'Constraints' finished: PASSED"
31.01.2018 22:56:07 - "Test Suite 'Conformance class: Application schema, Geographical Names' finished: PASSED"
31.01.2018 22:56:07 - Releasing resources
31.01.2018 22:56:07 - TestRunTask initialized
31.01.2018 22:56:07 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:56:07 - Validation ended with 0 error(s)
31.01.2018 22:56:07 - Compiling test script
31.01.2018 22:56:07 - Starting XQuery tests
31.01.2018 22:56:07 - "Testing 1 features"
31.01.2018 22:56:07 - "Indexing features (parsing errors: 0): 36 ms"
31.01.2018 22:56:07 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/data-consistency/ets-data-consistency-bsxets.xml"
31.01.2018 22:56:07 - "Statistics table: 1 ms"
31.01.2018 22:56:07 - "Test Suite 'Conformance class: Data consistency, General requirements' started"
31.01.2018 22:56:07 - "Test Case 'Version consistency' started"
31.01.2018 22:56:07 - "Test Assertion 'dc.a.1: Version lifespan plausible': PASSED - 0 ms"
31.01.2018 22:56:07 - "Test Assertion 'dc.a.2: Unique identifier persistency': PASSED_MANUAL"
31.01.2018 22:56:07 - "Test Assertion 'dc.a.3: Spatial object type stable': PASSED_MANUAL"
31.01.2018 22:56:07 - "Test Case 'Version consistency' finished: PASSED_MANUAL"
31.01.2018 22:56:07 - "Test Case 'Temporal consistency' started"
31.01.2018 22:56:07 - "Test Assertion 'dc.b.1: Valid time plausible': PASSED - 0 ms"
31.01.2018 22:56:07 - "Test Case 'Temporal consistency' finished: PASSED"
31.01.2018 22:56:07 - "Test Suite 'Conformance class: Data consistency, General requirements' finished: PASSED_MANUAL"
31.01.2018 22:56:08 - Releasing resources
31.01.2018 22:56:08 - TestRunTask initialized
31.01.2018 22:56:08 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:56:08 - Validation ended with 0 error(s)
31.01.2018 22:56:08 - Compiling test script
31.01.2018 22:56:08 - Starting XQuery tests
31.01.2018 22:56:08 - "Testing 1 features"
31.01.2018 22:56:08 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-gn/gn-dc/ets-gn-dc-bsxets.xml"
31.01.2018 22:56:08 - "Statistics table: 0 ms"
31.01.2018 22:56:08 - "Test Suite 'Conformance class: Data consistency, Geographical Names' started"
31.01.2018 22:56:08 - "Test Case 'Additional theme-specific consistency rules' started"
31.01.2018 22:56:08 - "Test Assertion 'gn-dc.a.1: Test always passes': PASSED - 0 ms"
31.01.2018 22:56:08 - "Test Case 'Additional theme-specific consistency rules' finished: PASSED"
31.01.2018 22:56:08 - "Test Suite 'Conformance class: Data consistency, Geographical Names' finished: PASSED"
31.01.2018 22:56:08 - Releasing resources
31.01.2018 22:56:08 - TestRunTask initialized
31.01.2018 22:56:08 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:56:08 - Validation ended with 0 error(s)
31.01.2018 22:56:08 - Compiling test script
31.01.2018 22:56:08 - Starting XQuery tests
31.01.2018 22:56:08 - "Testing 1 features"
31.01.2018 22:56:08 - "Indexing features (parsing errors: 0): 38 ms"
31.01.2018 22:56:08 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/information-accessibility/ets-information-accessibility-bsxets.xml"
31.01.2018 22:56:08 - "Statistics table: 0 ms"
31.01.2018 22:56:08 - "Test Suite 'Conformance class: Information accessibility, General requirements' started"
31.01.2018 22:56:08 - "Test Case 'Coordinate reference systems (CRS)' started"
31.01.2018 22:56:08 - "Test Assertion 'ia.a.1: CRS publicly accessible via HTTP': FAILED - 0 ms"
31.01.2018 22:56:08 - "Test Case 'Coordinate reference systems (CRS)' finished: FAILED"
31.01.2018 22:56:08 - "Test Suite 'Conformance class: Information accessibility, General requirements' finished: FAILED"
31.01.2018 22:56:09 - Releasing resources
31.01.2018 22:56:09 - TestRunTask initialized
31.01.2018 22:56:09 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:56:09 - Validation ended with 0 error(s)
31.01.2018 22:56:09 - Compiling test script
31.01.2018 22:56:09 - Starting XQuery tests
31.01.2018 22:56:09 - "Testing 1 features"
31.01.2018 22:56:09 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-gn/gn-ia/ets-gn-ia-bsxets.xml"
31.01.2018 22:56:09 - "Statistics table: 0 ms"
31.01.2018 22:56:09 - "Test Suite 'Conformance class: Information accessibility, Geographical Names' started"
31.01.2018 22:56:09 - "Test Case 'Code lists' started"
31.01.2018 22:56:09 - "Test Assertion 'gn-ia.a.1: Code list extensions accessible': PASSED - 0 ms"
31.01.2018 22:56:09 - "Test Case 'Code lists' finished: PASSED"
31.01.2018 22:56:09 - "Test Case 'Feature references' started"
31.01.2018 22:56:09 - "Test Assertion 'gn-ia.b.1: referenced features retrievable': PASSED - 0 ms"
31.01.2018 22:56:09 - "Test Case 'Feature references' finished: PASSED"
31.01.2018 22:56:09 - "Test Suite 'Conformance class: Information accessibility, Geographical Names' finished: PASSED"
31.01.2018 22:56:09 - Releasing resources
31.01.2018 22:56:09 - TestRunTask initialized
31.01.2018 22:56:09 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:56:09 - Validation ended with 0 error(s)
31.01.2018 22:56:09 - Compiling test script
31.01.2018 22:56:09 - Starting XQuery tests
31.01.2018 22:56:09 - "Testing 1 features"
31.01.2018 22:56:09 - "Indexing features (parsing errors: 0): 38 ms"
31.01.2018 22:56:09 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/reference-systems/ets-reference-systems-bsxets.xml"
31.01.2018 22:56:09 - "Statistics table: 0 ms"
31.01.2018 22:56:09 - "Test Suite 'Conformance class: Reference systems, General requirements' started"
31.01.2018 22:56:09 - "Test Case 'Spatial reference systems' started"
31.01.2018 22:56:09 - "Test Assertion 'rs.a.1: Spatial reference systems in feature geometries': FAILED - 0 ms"
31.01.2018 22:56:09 - "Test Assertion 'rs.a.2: Default spatial reference systems in feature collections': PASSED - 0 ms"
31.01.2018 22:56:09 - "Test Case 'Spatial reference systems' finished: FAILED"
31.01.2018 22:56:09 - "Test Case 'Temporal reference systems' started"
31.01.2018 22:56:09 - "Test Assertion 'rs.a.3: Temporal reference systems in features': PASSED - 0 ms"
31.01.2018 22:56:09 - "Test Case 'Temporal reference systems' finished: PASSED"
31.01.2018 22:56:09 - "Test Suite 'Conformance class: Reference systems, General requirements' finished: FAILED"
31.01.2018 22:56:09 - Releasing resources
31.01.2018 22:56:09 - TestRunTask initialized
31.01.2018 22:56:10 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:56:10 - Validation ended with 0 error(s)
31.01.2018 22:56:10 - Compiling test script
31.01.2018 22:56:10 - Starting XQuery tests
31.01.2018 22:56:10 - "Testing 1 features"
31.01.2018 22:56:10 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-gn/gn-rs/ets-gn-rs-bsxets.xml"
31.01.2018 22:56:10 - "Statistics table: 0 ms"
31.01.2018 22:56:10 - "Test Suite 'Conformance class: Reference systems, Geographical Names' started"
31.01.2018 22:56:10 - "Test Case 'Additional theme-specific rules for reference systems' started"
31.01.2018 22:56:10 - "Test Assertion 'gn-rs.a.1: Test always passes': PASSED - 0 ms"
31.01.2018 22:56:10 - "Test Case 'Additional theme-specific rules for reference systems' finished: PASSED"
31.01.2018 22:56:10 - "Test Suite 'Conformance class: Reference systems, Geographical Names' finished: PASSED"
31.01.2018 22:56:10 - Releasing resources
31.01.2018 22:56:10 - Changed state from INITIALIZED to RUNNING
31.01.2018 22:56:10 - Duration: 39sec
31.01.2018 22:56:10 - TestRun finished
31.01.2018 22:56:10 - Changed state from RUNNING to COMPLETED
