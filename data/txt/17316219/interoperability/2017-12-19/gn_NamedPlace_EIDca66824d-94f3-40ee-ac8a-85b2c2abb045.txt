19.12.2017 23:24:55 - Preparing Test Run interoperability_17316219_Geodetick&yacute; a kartografick&yacute; &uacute;stav Bratislava(GK&Uacute;)_gn:NamedPlace (initiated Tue Dec 19 23:24:55 CET 2017)
19.12.2017 23:24:55 - Resolving Executable Test Suite dependencies
19.12.2017 23:24:55 - Preparing 10 Test Task:
19.12.2017 23:24:55 -  TestTask 1 (4a752726-8a50-43ae-aedf-ce5c5dd62e9b)
19.12.2017 23:24:55 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML encoding (EID: 545f9e49-009b-4114-9333-7ca26413b5d4, V: 0.2.1 )'
19.12.2017 23:24:55 -  with parameters: 
19.12.2017 23:24:55 - etf.testcases = *
19.12.2017 23:24:55 -  TestTask 2 (f6a19854-068b-4453-bb50-730df3daa304)
19.12.2017 23:24:55 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.09820daf-62b2-4fa3-a95f-56a0d2b7c4d8'
19.12.2017 23:24:55 -  with parameters: 
19.12.2017 23:24:55 - etf.testcases = *
19.12.2017 23:24:55 -  TestTask 3 (a7c8d2a6-9c7f-4239-875c-c4ae5a5e7235)
19.12.2017 23:24:55 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.02b7b0cb-429a-4f4e-b0db-988464fb9496'
19.12.2017 23:24:55 -  with parameters: 
19.12.2017 23:24:55 - etf.testcases = *
19.12.2017 23:24:55 -  TestTask 4 (ce0eaeda-8786-4a30-8609-502b8676b0ec)
19.12.2017 23:24:55 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Application schema, Geographical Names (EID: 0fc46305-c623-422b-b7d7-251c3b86eb7f, V: 0.2.1 )'
19.12.2017 23:24:55 -  with parameters: 
19.12.2017 23:24:55 - etf.testcases = *
19.12.2017 23:24:55 -  TestTask 5 (169f3e26-3611-4ff7-88aa-7e79b66c729c)
19.12.2017 23:24:55 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
19.12.2017 23:24:55 -  with parameters: 
19.12.2017 23:24:55 - etf.testcases = *
19.12.2017 23:24:55 -  TestTask 6 (065bc11d-6eca-4908-8d45-0952d86f31ff)
19.12.2017 23:24:55 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Data consistency, Geographical Names (EID: a32f76c7-f1d3-4d70-83ef-d51d2545fa2e, V: 0.2.0 )'
19.12.2017 23:24:55 -  with parameters: 
19.12.2017 23:24:55 - etf.testcases = *
19.12.2017 23:24:55 -  TestTask 7 (98a7dad6-c301-4a69-9906-3c9c672f5a3f)
19.12.2017 23:24:55 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
19.12.2017 23:24:55 -  with parameters: 
19.12.2017 23:24:55 - etf.testcases = *
19.12.2017 23:24:55 -  TestTask 8 (6a202689-a179-4bfd-a92c-beed4eeebb08)
19.12.2017 23:24:55 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Geographical Names (EID: c3379b85-853e-4a35-8c3d-b64191d94587, V: 0.2.1 )'
19.12.2017 23:24:55 -  with parameters: 
19.12.2017 23:24:55 - etf.testcases = *
19.12.2017 23:24:55 -  TestTask 9 (546bc5f2-f9e7-424b-a1f8-d3a3a1dcac61)
19.12.2017 23:24:55 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
19.12.2017 23:24:55 -  with parameters: 
19.12.2017 23:24:55 - etf.testcases = *
19.12.2017 23:24:55 -  TestTask 10 (761344e4-2790-4fd7-80f6-6f0bc6e94bef)
19.12.2017 23:24:55 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Reference systems, Geographical Names (EID: 1620bd27-b881-48a2-bf2b-301541e035f4, V: 0.2.0 )'
19.12.2017 23:24:55 -  with parameters: 
19.12.2017 23:24:55 - etf.testcases = *
19.12.2017 23:24:55 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
19.12.2017 23:24:55 - Setting state to CREATED
19.12.2017 23:24:55 - Changed state from CREATED to INITIALIZING
19.12.2017 23:24:55 - Starting TestRun.ca66824d-94f3-40ee-ac8a-85b2c2abb045 at 2017-12-19T23:24:57+01:00
19.12.2017 23:24:57 - Changed state from INITIALIZING to INITIALIZED
19.12.2017 23:24:57 - TestRunTask initialized
19.12.2017 23:24:57 - Creating new tests databases to speed up tests.
19.12.2017 23:24:57 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 23:24:57 - Optimizing last database etf-tdb-8f7127dd-2264-4dc1-813c-069b3f20c3ff-0 
19.12.2017 23:24:57 - Import completed
19.12.2017 23:24:57 - Validation ended with 0 error(s)
19.12.2017 23:24:57 - Compiling test script
19.12.2017 23:24:57 - Starting XQuery tests
19.12.2017 23:24:57 - "Testing 1 features"
19.12.2017 23:24:57 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-encoding/inspire-gml/ets-inspire-gml-bsxets.xml"
19.12.2017 23:24:57 - "Statistics table: 1 ms"
19.12.2017 23:24:57 - "Test Suite 'Conformance class: INSPIRE GML encoding' started"
19.12.2017 23:24:57 - "Test Case 'Basic tests' started"
19.12.2017 23:24:57 - "Test Assertion 'gml.a.1: Errors loading the XML documents': PASSED - 0 ms"
19.12.2017 23:24:57 - "Test Assertion 'gml.a.2: Document root element': PASSED - 0 ms"
19.12.2017 23:24:57 - "Test Assertion 'gml.a.3: Character encoding': NOT_APPLICABLE"
19.12.2017 23:24:57 - "Test Case 'Basic tests' finished: PASSED"
19.12.2017 23:24:57 - "Test Suite 'Conformance class: INSPIRE GML encoding' finished: PASSED"
19.12.2017 23:24:57 - Releasing resources
19.12.2017 23:24:57 - TestRunTask initialized
19.12.2017 23:24:57 - Recreating new tests databases as the Test Object has changed!
19.12.2017 23:24:57 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 23:24:57 - Optimizing last database etf-tdb-8f7127dd-2264-4dc1-813c-069b3f20c3ff-0 
19.12.2017 23:24:58 - Import completed
19.12.2017 23:24:58 - Validation ended with 0 error(s)
19.12.2017 23:24:58 - Compiling test script
19.12.2017 23:24:58 - Starting XQuery tests
19.12.2017 23:24:58 - "Testing 1 features"
19.12.2017 23:24:58 - "Indexing features (parsing errors: 0): 44 ms"
19.12.2017 23:24:58 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/schemas/ets-schemas-bsxets.xml"
19.12.2017 23:24:58 - "Statistics table: 1 ms"
19.12.2017 23:24:58 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' started"
19.12.2017 23:24:58 - "Test Case 'Schema' started"
19.12.2017 23:24:58 - "Test Assertion 'gmlas.a.1: Mapping of source data to INSPIRE': PASSED_MANUAL"
19.12.2017 23:24:58 - "Test Assertion 'gmlas.a.2: Modelling of additional spatial object types': PASSED_MANUAL"
19.12.2017 23:24:58 - "Test Case 'Schema' finished: PASSED_MANUAL"
19.12.2017 23:24:58 - "Test Case 'Schema validation' started"
19.12.2017 23:24:58 - "Test Assertion 'gmlas.b.1: xsi:schemaLocation attribute': PASSED - 0 ms"
19.12.2017 23:24:58 - "Validating get.xml"
19.12.2017 23:25:06 - "Duration: 8581 ms. Errors: 0."
19.12.2017 23:25:06 - "Test Assertion 'gmlas.b.2: validate XML documents': PASSED - 8581 ms"
19.12.2017 23:25:06 - "Test Case 'Schema validation' finished: PASSED"
19.12.2017 23:25:06 - "Test Case 'GML model' started"
19.12.2017 23:25:06 - "Test Assertion 'gmlas.c.1: Consistency with the GML model': PASSED - 0 ms"
19.12.2017 23:25:06 - "Test Assertion 'gmlas.c.2: nilReason attributes require xsi:nil=true': PASSED - 1 ms"
19.12.2017 23:25:06 - "Test Assertion 'gmlas.c.3: nilReason values': PASSED - 0 ms"
19.12.2017 23:25:06 - "Test Case 'GML model' finished: PASSED"
19.12.2017 23:25:06 - "Test Case 'Simple features' started"
19.12.2017 23:25:06 - "Test Assertion 'gmlas.d.1: No spatial topology objects': PASSED - 0 ms"
19.12.2017 23:25:06 - "Test Assertion 'gmlas.d.2: No non-linear interpolation': PASSED - 0 ms"
19.12.2017 23:25:06 - "Test Assertion 'gmlas.d.3: Surface geometry elements': PASSED - 0 ms"
19.12.2017 23:25:06 - "Test Assertion 'gmlas.d.4: No non-planar interpolation': PASSED - 0 ms"
19.12.2017 23:25:06 - "Test Assertion 'gmlas.d.5: Geometry elements': PASSED - 1 ms"
19.12.2017 23:25:06 - "Test Assertion 'gmlas.d.6: Point coordinates in gml:pos': PASSED - 0 ms"
19.12.2017 23:25:06 - "Test Assertion 'gmlas.d.7: Curve/Surface coordinates in gml:posList': PASSED - 0 ms"
19.12.2017 23:25:06 - "Test Assertion 'gmlas.d.8: No array property elements': PASSED - 0 ms"
19.12.2017 23:25:06 - "Test Assertion 'gmlas.d.9: 1, 2 or 3 coordinate dimensions': PASSED - 0 ms"
19.12.2017 23:25:07 - "Test Assertion 'gmlas.d.10: Validate geometries (1)': PASSED - 149 ms"
19.12.2017 23:25:07 - "Test Assertion 'gmlas.d.11: Validate geometries (2)': PASSED - 0 ms"
19.12.2017 23:25:07 - "Test Case 'Simple features' finished: PASSED"
19.12.2017 23:25:07 - "Test Case 'Code list values in basic data types' started"
19.12.2017 23:25:07 - "Test Assertion 'gmlas.e.1: GrammaticalNumber attributes': PASSED - 5 ms"
19.12.2017 23:25:07 - "Test Assertion 'gmlas.e.2: GrammaticalGender attributes': PASSED - 13 ms"
19.12.2017 23:25:07 - "Test Assertion 'gmlas.e.3: NameStatus attributes': PASSED - 12 ms"
19.12.2017 23:25:07 - "Test Assertion 'gmlas.e.4: Nativeness attributes': PASSED - 5 ms"
19.12.2017 23:25:07 - "Test Case 'Code list values in basic data types' finished: PASSED"
19.12.2017 23:25:07 - "Test Case 'Constraints' started"
19.12.2017 23:25:07 - "Test Assertion 'gmlas.f.1: At least one of the two attributes pronunciationSoundLink and pronunciationIPA shall not be void': PASSED - 0 ms"
19.12.2017 23:25:07 - "Test Case 'Constraints' finished: PASSED"
19.12.2017 23:25:07 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' finished: PASSED_MANUAL"
19.12.2017 23:25:07 - Releasing resources
19.12.2017 23:25:07 - TestRunTask initialized
19.12.2017 23:25:07 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 23:25:07 - Validation ended with 0 error(s)
19.12.2017 23:25:07 - Compiling test script
19.12.2017 23:25:07 - Starting XQuery tests
19.12.2017 23:25:07 - "Testing 1 features"
19.12.2017 23:25:07 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-gn/gn-gml/ets-gn-gml-bsxets.xml"
19.12.2017 23:25:07 - "Statistics table: 3 ms"
19.12.2017 23:25:07 - "Test Suite 'Conformance class: GML application schemas, Geographical Names' started"
19.12.2017 23:25:07 - "Test Case 'Basic test' started"
19.12.2017 23:25:07 - "Test Assertion 'gn-gml.a.1: Geographical Names feature in dataset': PASSED - 0 ms"
19.12.2017 23:25:07 - "Test Case 'Basic test' finished: PASSED"
19.12.2017 23:25:07 - "Test Suite 'Conformance class: GML application schemas, Geographical Names' finished: PASSED"
19.12.2017 23:25:07 - Releasing resources
19.12.2017 23:25:07 - TestRunTask initialized
19.12.2017 23:25:07 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 23:25:07 - Validation ended with 0 error(s)
19.12.2017 23:25:07 - Compiling test script
19.12.2017 23:25:07 - Starting XQuery tests
19.12.2017 23:25:07 - "Testing 1 features"
19.12.2017 23:25:07 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-gn/gn-as/ets-gn-as-bsxets.xml"
19.12.2017 23:25:07 - "Statistics table: 0 ms"
19.12.2017 23:25:07 - "Test Suite 'Conformance class: Application schema, Geographical Names' started"
19.12.2017 23:25:07 - "Test Case 'Code list values' started"
19.12.2017 23:25:07 - "Test Assertion 'gn-as.a.1: NamedPlaceType attributes': PASSED - 5 ms"
19.12.2017 23:25:07 - "Test Case 'Code list values' finished: PASSED"
19.12.2017 23:25:07 - "Test Case 'Constraints' started"
19.12.2017 23:25:07 - "Test Assertion 'gn-as.b.1: Test always passes': PASSED - 0 ms"
19.12.2017 23:25:07 - "Test Case 'Constraints' finished: PASSED"
19.12.2017 23:25:07 - "Test Suite 'Conformance class: Application schema, Geographical Names' finished: PASSED"
19.12.2017 23:25:08 - Releasing resources
19.12.2017 23:25:08 - TestRunTask initialized
19.12.2017 23:25:08 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 23:25:08 - Validation ended with 0 error(s)
19.12.2017 23:25:08 - Compiling test script
19.12.2017 23:25:08 - Starting XQuery tests
19.12.2017 23:25:08 - "Testing 1 features"
19.12.2017 23:25:08 - "Indexing features (parsing errors: 0): 43 ms"
19.12.2017 23:25:08 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/data-consistency/ets-data-consistency-bsxets.xml"
19.12.2017 23:25:08 - "Statistics table: 0 ms"
19.12.2017 23:25:08 - "Test Suite 'Conformance class: Data consistency, General requirements' started"
19.12.2017 23:25:08 - "Test Case 'Version consistency' started"
19.12.2017 23:25:08 - "Test Assertion 'dc.a.1: Version lifespan plausible': PASSED - 0 ms"
19.12.2017 23:25:08 - "Test Assertion 'dc.a.2: Unique identifier persistency': PASSED_MANUAL"
19.12.2017 23:25:08 - "Test Assertion 'dc.a.3: Spatial object type stable': PASSED_MANUAL"
19.12.2017 23:25:08 - "Test Case 'Version consistency' finished: PASSED_MANUAL"
19.12.2017 23:25:08 - "Test Case 'Temporal consistency' started"
19.12.2017 23:25:08 - "Test Assertion 'dc.b.1: Valid time plausible': PASSED - 0 ms"
19.12.2017 23:25:08 - "Test Case 'Temporal consistency' finished: PASSED"
19.12.2017 23:25:08 - "Test Suite 'Conformance class: Data consistency, General requirements' finished: PASSED_MANUAL"
19.12.2017 23:25:08 - Releasing resources
19.12.2017 23:25:08 - TestRunTask initialized
19.12.2017 23:25:08 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 23:25:08 - Validation ended with 0 error(s)
19.12.2017 23:25:08 - Compiling test script
19.12.2017 23:25:08 - Starting XQuery tests
19.12.2017 23:25:08 - "Testing 1 features"
19.12.2017 23:25:08 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-gn/gn-dc/ets-gn-dc-bsxets.xml"
19.12.2017 23:25:08 - "Statistics table: 1 ms"
19.12.2017 23:25:08 - "Test Suite 'Conformance class: Data consistency, Geographical Names' started"
19.12.2017 23:25:08 - "Test Case 'Additional theme-specific consistency rules' started"
19.12.2017 23:25:08 - "Test Assertion 'gn-dc.a.1: Test always passes': PASSED - 0 ms"
19.12.2017 23:25:08 - "Test Case 'Additional theme-specific consistency rules' finished: PASSED"
19.12.2017 23:25:08 - "Test Suite 'Conformance class: Data consistency, Geographical Names' finished: PASSED"
19.12.2017 23:25:08 - Releasing resources
19.12.2017 23:25:08 - TestRunTask initialized
19.12.2017 23:25:08 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 23:25:08 - Validation ended with 0 error(s)
19.12.2017 23:25:08 - Compiling test script
19.12.2017 23:25:08 - Starting XQuery tests
19.12.2017 23:25:08 - "Testing 1 features"
19.12.2017 23:25:08 - "Indexing features (parsing errors: 0): 39 ms"
19.12.2017 23:25:08 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/information-accessibility/ets-information-accessibility-bsxets.xml"
19.12.2017 23:25:08 - "Statistics table: 0 ms"
19.12.2017 23:25:08 - "Test Suite 'Conformance class: Information accessibility, General requirements' started"
19.12.2017 23:25:08 - "Test Case 'Coordinate reference systems (CRS)' started"
19.12.2017 23:25:08 - "Test Assertion 'ia.a.1: CRS publicly accessible via HTTP': FAILED - 1 ms"
19.12.2017 23:25:08 - "Test Case 'Coordinate reference systems (CRS)' finished: FAILED"
19.12.2017 23:25:08 - "Test Suite 'Conformance class: Information accessibility, General requirements' finished: FAILED"
19.12.2017 23:25:09 - Releasing resources
19.12.2017 23:25:09 - TestRunTask initialized
19.12.2017 23:25:09 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 23:25:09 - Validation ended with 0 error(s)
19.12.2017 23:25:09 - Compiling test script
19.12.2017 23:25:09 - Starting XQuery tests
19.12.2017 23:25:09 - "Testing 1 features"
19.12.2017 23:25:09 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-gn/gn-ia/ets-gn-ia-bsxets.xml"
19.12.2017 23:25:09 - "Statistics table: 1 ms"
19.12.2017 23:25:09 - "Test Suite 'Conformance class: Information accessibility, Geographical Names' started"
19.12.2017 23:25:09 - "Test Case 'Code lists' started"
19.12.2017 23:25:09 - "Test Assertion 'gn-ia.a.1: Code list extensions accessible': PASSED - 0 ms"
19.12.2017 23:25:09 - "Test Case 'Code lists' finished: PASSED"
19.12.2017 23:25:09 - "Test Case 'Feature references' started"
19.12.2017 23:25:09 - "Test Assertion 'gn-ia.b.1: referenced features retrievable': PASSED - 0 ms"
19.12.2017 23:25:09 - "Test Case 'Feature references' finished: PASSED"
19.12.2017 23:25:09 - "Test Suite 'Conformance class: Information accessibility, Geographical Names' finished: PASSED"
19.12.2017 23:25:09 - Releasing resources
19.12.2017 23:25:09 - TestRunTask initialized
19.12.2017 23:25:09 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 23:25:09 - Validation ended with 0 error(s)
19.12.2017 23:25:09 - Compiling test script
19.12.2017 23:25:09 - Starting XQuery tests
19.12.2017 23:25:09 - "Testing 1 features"
19.12.2017 23:25:09 - "Indexing features (parsing errors: 0): 40 ms"
19.12.2017 23:25:09 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/reference-systems/ets-reference-systems-bsxets.xml"
19.12.2017 23:25:09 - "Statistics table: 1 ms"
19.12.2017 23:25:09 - "Test Suite 'Conformance class: Reference systems, General requirements' started"
19.12.2017 23:25:09 - "Test Case 'Spatial reference systems' started"
19.12.2017 23:25:09 - "Test Assertion 'rs.a.1: Spatial reference systems in feature geometries': FAILED - 1 ms"
19.12.2017 23:25:09 - "Test Assertion 'rs.a.2: Default spatial reference systems in feature collections': PASSED - 0 ms"
19.12.2017 23:25:09 - "Test Case 'Spatial reference systems' finished: FAILED"
19.12.2017 23:25:09 - "Test Case 'Temporal reference systems' started"
19.12.2017 23:25:09 - "Test Assertion 'rs.a.3: Temporal reference systems in features': PASSED - 0 ms"
19.12.2017 23:25:09 - "Test Case 'Temporal reference systems' finished: PASSED"
19.12.2017 23:25:09 - "Test Suite 'Conformance class: Reference systems, General requirements' finished: FAILED"
19.12.2017 23:25:09 - Releasing resources
19.12.2017 23:25:09 - TestRunTask initialized
19.12.2017 23:25:09 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 23:25:09 - Validation ended with 0 error(s)
19.12.2017 23:25:09 - Compiling test script
19.12.2017 23:25:09 - Starting XQuery tests
19.12.2017 23:25:09 - "Testing 1 features"
19.12.2017 23:25:09 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-gn/gn-rs/ets-gn-rs-bsxets.xml"
19.12.2017 23:25:09 - "Statistics table: 1 ms"
19.12.2017 23:25:09 - "Test Suite 'Conformance class: Reference systems, Geographical Names' started"
19.12.2017 23:25:09 - "Test Case 'Additional theme-specific rules for reference systems' started"
19.12.2017 23:25:09 - "Test Assertion 'gn-rs.a.1: Test always passes': PASSED - 0 ms"
19.12.2017 23:25:09 - "Test Case 'Additional theme-specific rules for reference systems' finished: PASSED"
19.12.2017 23:25:09 - "Test Suite 'Conformance class: Reference systems, Geographical Names' finished: PASSED"
19.12.2017 23:25:10 - Releasing resources
19.12.2017 23:25:10 - Changed state from INITIALIZED to RUNNING
19.12.2017 23:25:10 - Duration: 14sec
19.12.2017 23:25:10 - TestRun finished
19.12.2017 23:25:10 - Changed state from RUNNING to COMPLETED
