15.11.2017 15:34:15 - Preparing Test Run metadata (initiated Wed Nov 15 15:34:14 CET 2017)
15.11.2017 15:34:15 - Resolving Executable Test Suite dependencies
15.11.2017 15:34:15 - Preparing 2 Test Task:
15.11.2017 15:34:15 -  TestTask 1 (9f5c7b65-c645-4d8b-a065-6fcc0b3f57bb)
15.11.2017 15:34:15 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
15.11.2017 15:34:15 -  with parameters: 
15.11.2017 15:34:15 - etf.testcases = *
15.11.2017 15:34:15 -  TestTask 2 (e6047b3a-0122-4fd2-ae7e-bb3561742461)
15.11.2017 15:34:15 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.4 )'
15.11.2017 15:34:15 -  with parameters: 
15.11.2017 15:34:15 - etf.testcases = *
15.11.2017 15:34:15 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
15.11.2017 15:34:15 - Setting state to CREATED
15.11.2017 15:34:15 - Changed state from CREATED to INITIALIZING
15.11.2017 15:34:15 - Starting TestRun.bb33ae6f-1786-436f-9edd-7e36a424ea7d at 2017-11-15T15:34:16+01:00
15.11.2017 15:34:16 - Changed state from INITIALIZING to INITIALIZED
15.11.2017 15:34:16 - TestRunTask initialized
15.11.2017 15:34:16 - Creating new tests databases to speed up tests.
15.11.2017 15:34:16 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 15:34:16 - Optimizing last database etf-tdb-8a8f3bcc-3e61-4a45-8a60-8f7473bd167a-0 
15.11.2017 15:34:16 - Import completed
15.11.2017 15:34:17 - Validation ended with 0 error(s)
15.11.2017 15:34:17 - Compiling test script
15.11.2017 15:34:17 - Starting XQuery tests
15.11.2017 15:34:17 - "Testing 1 records"
15.11.2017 15:34:17 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
15.11.2017 15:34:17 - "Statistics table: 1 ms"
15.11.2017 15:34:17 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
15.11.2017 15:34:17 - "Test Case 'Schema validation' started"
15.11.2017 15:34:20 - "Validating file GetRecordByIdResponse.xml: 3590 ms"
15.11.2017 15:34:20 - "Test Assertion 'md-xml.a.1: Validate XML documents': PASSED - 3591 ms"
15.11.2017 15:34:20 - "Test Case 'Schema validation' finished: PASSED"
15.11.2017 15:34:20 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: PASSED"
15.11.2017 15:34:21 - Releasing resources
15.11.2017 15:34:21 - TestRunTask initialized
15.11.2017 15:34:21 - Recreating new tests databases as the Test Object has changed!
15.11.2017 15:34:21 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 15:34:21 - Optimizing last database etf-tdb-8a8f3bcc-3e61-4a45-8a60-8f7473bd167a-0 
15.11.2017 15:34:21 - Import completed
15.11.2017 15:34:21 - Validation ended with 0 error(s)
15.11.2017 15:34:21 - Compiling test script
15.11.2017 15:34:22 - Starting XQuery tests
15.11.2017 15:34:22 - "Testing 1 records"
15.11.2017 15:34:22 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
15.11.2017 15:34:22 - "Statistics table: 0 ms"
15.11.2017 15:34:22 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
15.11.2017 15:34:22 - "Test Case 'Common tests' started"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.a.1: Title': PASSED - 0 ms"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.a.2: Abstract': PASSED - 0 ms"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.a.3: Access and use conditions': PASSED_MANUAL - 0 ms"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.a.4: Public access': PASSED - 0 ms"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 0 ms"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.a.6: Language': PASSED - 0 ms"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 0 ms"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 0 ms"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.a.10: Responsible party contact info': PASSED - 1 ms"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.a.11: Responsible party role': PASSED - 0 ms"
15.11.2017 15:34:22 - "Test Case 'Common tests' finished: PASSED_MANUAL"
15.11.2017 15:34:22 - "Test Case 'Hierarchy level' started"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 0 ms"
15.11.2017 15:34:22 - "Test Case 'Hierarchy level' finished: PASSED"
15.11.2017 15:34:22 - "Test Case 'Dataset (series) tests' started"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.c.1: Dataset identification': PASSED - 0 ms"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.c.2: Dataset language': PASSED - 0 ms"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.c.3: Dataset linkage': PASSED - 0 ms"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.c.4: Dataset conformity': PASSED - 0 ms"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.c.5: Dataset topic': PASSED - 0 ms"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': PASSED - 0 ms"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.c.7: Dataset lineage': PASSED - 0 ms"
15.11.2017 15:34:22 - "Test Case 'Dataset (series) tests' finished: PASSED"
15.11.2017 15:34:22 - "Test Case 'Service tests' started"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.d.1: Service type': PASSED - 0 ms"
15.11.2017 15:34:22 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_geographical_names_wfs/service.svc/get'"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED_MANUAL - 189 ms"
15.11.2017 15:34:22 - "Checking URL: 'https://zbgisws.skgeodesy.sk/zbgiscsw/service.svc/get?REQUEST=GetRecordById&amp;SERVICE=CSW&amp;VERSION=2.0.2&amp;OUTPUTSCHEMA=http://www.isotc211.org/2005/gmd&amp;ELEMENTSETNAME=full&amp;Id=https://data.gov.sk/set/rpi/gmd/17316219/SK_UGKK_ZBGIS_INSPIRE_GN'"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.d.3: Coupled resource': PASSED - 672 ms"
15.11.2017 15:34:22 - "Test Case 'Service tests' finished: PASSED_MANUAL"
15.11.2017 15:34:22 - "Test Case 'Keywords' started"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.e.1: Keywords': PASSED - 0 ms"
15.11.2017 15:34:22 - "Test Case 'Keywords' finished: PASSED"
15.11.2017 15:34:22 - "Test Case 'Keywords - details' started"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 1 ms"
15.11.2017 15:34:22 - "Checking URL: 'http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/SpatialDataServiceCategory.en.atom'"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 7 ms"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 0 ms"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 0 ms"
15.11.2017 15:34:22 - "Test Case 'Keywords - details' finished: PASSED"
15.11.2017 15:34:22 - "Test Case 'Temporal extent' started"
15.11.2017 15:34:22 - "Test Assertion 'md-iso.g.1: Temporal extent': PASSED - 0 ms"
15.11.2017 15:34:22 - "Test Case 'Temporal extent' finished: PASSED"
15.11.2017 15:34:23 - "Test Case 'Temporal extent - details' started"
15.11.2017 15:34:23 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 0 ms"
15.11.2017 15:34:23 - "Test Case 'Temporal extent - details' finished: PASSED"
15.11.2017 15:34:23 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: PASSED_MANUAL"
15.11.2017 15:34:24 - Releasing resources
15.11.2017 15:34:24 - Changed state from INITIALIZED to RUNNING
15.11.2017 15:34:24 - Duration: 9sec
15.11.2017 15:34:24 - TestRun finished
15.11.2017 15:34:24 - Changed state from RUNNING to COMPLETED
