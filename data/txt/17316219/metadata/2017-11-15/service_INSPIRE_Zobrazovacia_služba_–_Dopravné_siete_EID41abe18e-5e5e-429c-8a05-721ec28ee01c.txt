15.11.2017 16:43:03 - Preparing Test Run metadata (initiated Wed Nov 15 16:43:03 CET 2017)
15.11.2017 16:43:03 - Resolving Executable Test Suite dependencies
15.11.2017 16:43:03 - Preparing 2 Test Task:
15.11.2017 16:43:03 -  TestTask 1 (14fca5e3-fcb5-4eef-bdb1-c07fe40a9909)
15.11.2017 16:43:03 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
15.11.2017 16:43:03 -  with parameters: 
15.11.2017 16:43:03 - etf.testcases = *
15.11.2017 16:43:03 -  TestTask 2 (7cb08675-a253-4654-a76c-9fc743047988)
15.11.2017 16:43:03 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.4 )'
15.11.2017 16:43:03 -  with parameters: 
15.11.2017 16:43:03 - etf.testcases = *
15.11.2017 16:43:03 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
15.11.2017 16:43:03 - Setting state to CREATED
15.11.2017 16:43:03 - Changed state from CREATED to INITIALIZING
15.11.2017 16:43:03 - Starting TestRun.41abe18e-5e5e-429c-8a05-721ec28ee01c at 2017-11-15T16:43:04+01:00
15.11.2017 16:43:04 - Changed state from INITIALIZING to INITIALIZED
15.11.2017 16:43:04 - TestRunTask initialized
15.11.2017 16:43:04 - Creating new tests databases to speed up tests.
15.11.2017 16:43:04 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 16:43:04 - Optimizing last database etf-tdb-c45252a3-5324-477d-aa62-21346fb236b6-0 
15.11.2017 16:43:04 - Import completed
15.11.2017 16:43:05 - Validation ended with 0 error(s)
15.11.2017 16:43:05 - Compiling test script
15.11.2017 16:43:05 - Starting XQuery tests
15.11.2017 16:43:05 - "Testing 1 records"
15.11.2017 16:43:05 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
15.11.2017 16:43:05 - "Statistics table: 1 ms"
15.11.2017 16:43:05 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
15.11.2017 16:43:05 - "Test Case 'Schema validation' started"
15.11.2017 16:43:07 - "Validating file GetRecordByIdResponse.xml: 1976 ms"
15.11.2017 16:43:07 - "Test Assertion 'md-xml.a.1: Validate XML documents': PASSED - 1976 ms"
15.11.2017 16:43:07 - "Test Case 'Schema validation' finished: PASSED"
15.11.2017 16:43:07 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: PASSED"
15.11.2017 16:43:08 - Releasing resources
15.11.2017 16:43:08 - TestRunTask initialized
15.11.2017 16:43:08 - Recreating new tests databases as the Test Object has changed!
15.11.2017 16:43:08 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
15.11.2017 16:43:08 - Optimizing last database etf-tdb-c45252a3-5324-477d-aa62-21346fb236b6-0 
15.11.2017 16:43:08 - Import completed
15.11.2017 16:43:08 - Validation ended with 0 error(s)
15.11.2017 16:43:08 - Compiling test script
15.11.2017 16:43:08 - Starting XQuery tests
15.11.2017 16:43:08 - "Testing 1 records"
15.11.2017 16:43:08 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
15.11.2017 16:43:08 - "Statistics table: 1 ms"
15.11.2017 16:43:08 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
15.11.2017 16:43:08 - "Test Case 'Common tests' started"
15.11.2017 16:43:08 - "Test Assertion 'md-iso.a.1: Title': PASSED - 0 ms"
15.11.2017 16:43:08 - "Test Assertion 'md-iso.a.2: Abstract': PASSED - 0 ms"
15.11.2017 16:43:08 - "Test Assertion 'md-iso.a.3: Access and use conditions': PASSED_MANUAL - 0 ms"
15.11.2017 16:43:08 - "Test Assertion 'md-iso.a.4: Public access': PASSED - 0 ms"
15.11.2017 16:43:08 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 0 ms"
15.11.2017 16:43:08 - "Test Assertion 'md-iso.a.6: Language': PASSED - 0 ms"
15.11.2017 16:43:08 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 0 ms"
15.11.2017 16:43:08 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
15.11.2017 16:43:08 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 0 ms"
15.11.2017 16:43:08 - "Test Assertion 'md-iso.a.10: Responsible party contact info': PASSED - 0 ms"
15.11.2017 16:43:08 - "Test Assertion 'md-iso.a.11: Responsible party role': PASSED - 0 ms"
15.11.2017 16:43:08 - "Test Case 'Common tests' finished: PASSED_MANUAL"
15.11.2017 16:43:08 - "Test Case 'Hierarchy level' started"
15.11.2017 16:43:08 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 0 ms"
15.11.2017 16:43:08 - "Test Case 'Hierarchy level' finished: PASSED"
15.11.2017 16:43:08 - "Test Case 'Dataset (series) tests' started"
15.11.2017 16:43:08 - "Test Assertion 'md-iso.c.1: Dataset identification': PASSED - 0 ms"
15.11.2017 16:43:08 - "Test Assertion 'md-iso.c.2: Dataset language': PASSED - 0 ms"
15.11.2017 16:43:08 - "Test Assertion 'md-iso.c.3: Dataset linkage': PASSED - 0 ms"
15.11.2017 16:43:08 - "Test Assertion 'md-iso.c.4: Dataset conformity': PASSED - 0 ms"
15.11.2017 16:43:08 - "Test Assertion 'md-iso.c.5: Dataset topic': PASSED - 0 ms"
15.11.2017 16:43:08 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': PASSED - 0 ms"
15.11.2017 16:43:08 - "Test Assertion 'md-iso.c.7: Dataset lineage': PASSED - 0 ms"
15.11.2017 16:43:08 - "Test Case 'Dataset (series) tests' finished: PASSED"
15.11.2017 16:43:08 - "Test Case 'Service tests' started"
15.11.2017 16:43:08 - "Test Assertion 'md-iso.d.1: Service type': PASSED - 0 ms"
15.11.2017 16:43:08 - "Checking URL: 'https://zbgis.skgeodesy.sk/tkgis/?addWMS=https://zbgisws.skgeodesy.sk/inspire_transport_networks_wms/service.svc/get'"
15.11.2017 16:43:08 - "Checking URL: 'https://zbgis.skgeodesy.sk/mkzbgis/'"
15.11.2017 16:43:08 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_transport_networks_wms/service.svc/get'"
15.11.2017 16:43:08 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED_MANUAL - 477 ms"
15.11.2017 16:43:08 - "Checking URL: 'https://zbgisws.skgeodesy.sk/zbgiscsw/service.svc/get?REQUEST=GetRecordById&amp;SERVICE=CSW&amp;VERSION=2.0.2&amp;OUTPUTSCHEMA=http://www.isotc211.org/2005/gmd&amp;ELEMENTSETNAME=full&amp;Id=https://data.gov.sk/set/rpi/gmd/17316219/SK_UGKK_ZBGIS_INSPIRE_TN'"
15.11.2017 16:43:09 - "Test Assertion 'md-iso.d.3: Coupled resource': PASSED - 456 ms"
15.11.2017 16:43:09 - "Test Case 'Service tests' finished: PASSED_MANUAL"
15.11.2017 16:43:09 - "Test Case 'Keywords' started"
15.11.2017 16:43:09 - "Test Assertion 'md-iso.e.1: Keywords': PASSED - 0 ms"
15.11.2017 16:43:09 - "Test Case 'Keywords' finished: PASSED"
15.11.2017 16:43:09 - "Test Case 'Keywords - details' started"
15.11.2017 16:43:09 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 0 ms"
15.11.2017 16:43:09 - "Checking URL: 'http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/SpatialDataServiceCategory.en.atom'"
15.11.2017 16:43:09 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 7 ms"
15.11.2017 16:43:09 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 1 ms"
15.11.2017 16:43:09 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 0 ms"
15.11.2017 16:43:09 - "Test Case 'Keywords - details' finished: PASSED"
15.11.2017 16:43:09 - "Test Case 'Temporal extent' started"
15.11.2017 16:43:09 - "Test Assertion 'md-iso.g.1: Temporal extent': PASSED - 0 ms"
15.11.2017 16:43:09 - "Test Case 'Temporal extent' finished: PASSED"
15.11.2017 16:43:09 - "Test Case 'Temporal extent - details' started"
15.11.2017 16:43:09 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 0 ms"
15.11.2017 16:43:09 - "Test Case 'Temporal extent - details' finished: PASSED"
15.11.2017 16:43:09 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: PASSED_MANUAL"
15.11.2017 16:43:10 - Releasing resources
15.11.2017 16:43:10 - Changed state from INITIALIZED to RUNNING
15.11.2017 16:43:10 - Duration: 7sec
15.11.2017 16:43:10 - TestRun finished
15.11.2017 16:43:10 - Changed state from RUNNING to COMPLETED
