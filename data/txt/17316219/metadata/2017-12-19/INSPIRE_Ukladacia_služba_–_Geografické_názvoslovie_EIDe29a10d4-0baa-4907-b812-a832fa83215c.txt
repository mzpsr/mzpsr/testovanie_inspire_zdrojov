19.12.2017 23:40:48 - Preparing Test Run metadata_17316219_Geodetick&yacute; a kartografick&yacute; &uacute;stav Bratislava(GK&Uacute;)_INSPIRE Ukladacia služba &ndash; Geografick&eacute; n&aacute;zvoslovie (initiated Tue Dec 19 23:40:48 CET 2017)
19.12.2017 23:40:48 - Resolving Executable Test Suite dependencies
19.12.2017 23:40:48 - Preparing 2 Test Task:
19.12.2017 23:40:48 -  TestTask 1 (e97140b9-d1ef-4be1-9d46-4b0ff4fb1c36)
19.12.2017 23:40:48 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
19.12.2017 23:40:48 -  with parameters: 
19.12.2017 23:40:48 - etf.testcases = *
19.12.2017 23:40:48 -  TestTask 2 (3a652fe3-5b51-4668-b45f-adff698410e0)
19.12.2017 23:40:48 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.5 )'
19.12.2017 23:40:48 -  with parameters: 
19.12.2017 23:40:48 - etf.testcases = *
19.12.2017 23:40:48 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
19.12.2017 23:40:48 - Setting state to CREATED
19.12.2017 23:40:48 - Changed state from CREATED to INITIALIZING
19.12.2017 23:40:48 - Starting TestRun.e29a10d4-0baa-4907-b812-a832fa83215c at 2017-12-19T23:40:50+01:00
19.12.2017 23:40:50 - Changed state from INITIALIZING to INITIALIZED
19.12.2017 23:40:50 - TestRunTask initialized
19.12.2017 23:40:50 - Creating new tests databases to speed up tests.
19.12.2017 23:40:50 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 23:40:50 - Optimizing last database etf-tdb-c5cd105b-84d3-4846-922a-d9f521196a3d-0 
19.12.2017 23:40:50 - Import completed
19.12.2017 23:40:50 - Validation ended with 0 error(s)
19.12.2017 23:40:50 - Compiling test script
19.12.2017 23:40:50 - Starting XQuery tests
19.12.2017 23:40:50 - "Testing 1 records"
19.12.2017 23:40:50 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
19.12.2017 23:40:50 - "Statistics table: 1 ms"
19.12.2017 23:40:50 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
19.12.2017 23:40:50 - "Test Case 'Schema validation' started"
19.12.2017 23:40:51 - "Validating file GetRecordByIdResponse.xml: 1411 ms"
19.12.2017 23:40:51 - "Test Assertion 'md-xml.a.1: Validate XML documents': PASSED - 1411 ms"
19.12.2017 23:40:51 - "Test Case 'Schema validation' finished: PASSED"
19.12.2017 23:40:51 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: PASSED"
19.12.2017 23:40:51 - Releasing resources
19.12.2017 23:40:51 - TestRunTask initialized
19.12.2017 23:40:51 - Recreating new tests databases as the Test Object has changed!
19.12.2017 23:40:51 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 23:40:51 - Optimizing last database etf-tdb-c5cd105b-84d3-4846-922a-d9f521196a3d-0 
19.12.2017 23:40:51 - Import completed
19.12.2017 23:40:51 - Validation ended with 0 error(s)
19.12.2017 23:40:51 - Compiling test script
19.12.2017 23:40:51 - Starting XQuery tests
19.12.2017 23:40:52 - "Testing 1 records"
19.12.2017 23:40:52 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
19.12.2017 23:40:52 - "Statistics table: 1 ms"
19.12.2017 23:40:52 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
19.12.2017 23:40:52 - "Test Case 'Common tests' started"
19.12.2017 23:40:52 - "Test Assertion 'md-iso.a.1: Title': PASSED - 0 ms"
19.12.2017 23:40:52 - "Test Assertion 'md-iso.a.2: Abstract': PASSED - 1 ms"
19.12.2017 23:40:52 - "Test Assertion 'md-iso.a.3: Access and use conditions': PASSED_MANUAL - 0 ms"
19.12.2017 23:40:52 - "Test Assertion 'md-iso.a.4: Public access': PASSED - 0 ms"
19.12.2017 23:40:52 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 0 ms"
19.12.2017 23:40:52 - "Test Assertion 'md-iso.a.6: Language': PASSED - 0 ms"
19.12.2017 23:40:52 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 1 ms"
19.12.2017 23:40:52 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
19.12.2017 23:40:52 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 0 ms"
19.12.2017 23:40:52 - "Test Assertion 'md-iso.a.10: Responsible party contact info': PASSED - 0 ms"
19.12.2017 23:40:52 - "Test Assertion 'md-iso.a.11: Responsible party role': PASSED - 0 ms"
19.12.2017 23:40:52 - "Test Case 'Common tests' finished: PASSED_MANUAL"
19.12.2017 23:40:52 - "Test Case 'Hierarchy level' started"
19.12.2017 23:40:52 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 0 ms"
19.12.2017 23:40:52 - "Test Case 'Hierarchy level' finished: PASSED"
19.12.2017 23:40:52 - "Test Case 'Dataset (series) tests' started"
19.12.2017 23:40:52 - "Test Assertion 'md-iso.c.1: Dataset identification': PASSED - 0 ms"
19.12.2017 23:40:52 - "Test Assertion 'md-iso.c.2: Dataset language': PASSED - 0 ms"
19.12.2017 23:40:52 - "Test Assertion 'md-iso.c.3: Dataset linkage': PASSED - 0 ms"
19.12.2017 23:40:52 - "Test Assertion 'md-iso.c.4: Dataset conformity': PASSED - 0 ms"
19.12.2017 23:40:52 - "Test Assertion 'md-iso.c.5: Dataset topic': PASSED - 0 ms"
19.12.2017 23:40:52 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': PASSED - 0 ms"
19.12.2017 23:40:52 - "Test Assertion 'md-iso.c.7: Dataset lineage': PASSED - 0 ms"
19.12.2017 23:40:52 - "Test Case 'Dataset (series) tests' finished: PASSED"
19.12.2017 23:40:52 - "Test Case 'Service tests' started"
19.12.2017 23:40:52 - "Test Assertion 'md-iso.d.1: Service type': PASSED - 0 ms"
19.12.2017 23:40:52 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_geographical_names_wfs/service.svc/get'"
19.12.2017 23:40:52 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED_MANUAL - 145 ms"
19.12.2017 23:40:52 - "Checking URL: 'https://zbgisws.skgeodesy.sk/zbgiscsw/service.svc/get?REQUEST=GetRecordById&amp;SERVICE=CSW&amp;VERSION=2.0.2&amp;OUTPUTSCHEMA=http://www.isotc211.org/2005/gmd&amp;ELEMENTSETNAME=full&amp;Id=https://data.gov.sk/set/rpi/gmd/17316219/SK_UGKK_..."
19.12.2017 23:41:28 - "Test Assertion 'md-iso.d.3: Coupled resource': FAILED - 36467 ms"
19.12.2017 23:41:28 - "Test Case 'Service tests' finished: FAILED"
19.12.2017 23:41:28 - "Test Case 'Keywords' started"
19.12.2017 23:41:28 - "Test Assertion 'md-iso.e.1: Keywords': PASSED - 0 ms"
19.12.2017 23:41:28 - "Test Case 'Keywords' finished: PASSED"
19.12.2017 23:41:28 - "Test Case 'Keywords - details' started"
19.12.2017 23:41:28 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 0 ms"
19.12.2017 23:41:28 - "Checking URL: 'http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/SpatialDataServiceCategory.en.atom'"
19.12.2017 23:41:29 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 466 ms"
19.12.2017 23:41:29 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 0 ms"
19.12.2017 23:41:29 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 0 ms"
19.12.2017 23:41:29 - "Test Case 'Keywords - details' finished: PASSED"
19.12.2017 23:41:29 - "Test Case 'Temporal extent' started"
19.12.2017 23:41:29 - "Test Assertion 'md-iso.g.1: Temporal extent': PASSED - 0 ms"
19.12.2017 23:41:29 - "Test Case 'Temporal extent' finished: PASSED"
19.12.2017 23:41:29 - "Test Case 'Temporal extent - details' started"
19.12.2017 23:41:29 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 0 ms"
19.12.2017 23:41:29 - "Test Case 'Temporal extent - details' finished: PASSED"
19.12.2017 23:41:29 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: FAILED"
19.12.2017 23:41:29 - Releasing resources
19.12.2017 23:41:29 - Changed state from INITIALIZED to RUNNING
19.12.2017 23:41:29 - Duration: 41sec
19.12.2017 23:41:29 - TestRun finished
19.12.2017 23:41:29 - Changed state from RUNNING to COMPLETED
