22.01.2018 22:44:14 - Preparing Test Run metadata_17316219_Geodetick&yacute; a kartografick&yacute; &uacute;stav Bratislava_INSPIRE - Dopravn&eacute; siete (initiated Mon Jan 22 22:44:14 CET 2018)
22.01.2018 22:44:14 - Resolving Executable Test Suite dependencies
22.01.2018 22:44:14 - Preparing 2 Test Task:
22.01.2018 22:44:14 -  TestTask 1 (6ac1c28a-c58b-4524-8b02-7f8c528f7d88)
22.01.2018 22:44:14 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
22.01.2018 22:44:14 -  with parameters: 
22.01.2018 22:44:14 - etf.testcases = *
22.01.2018 22:44:14 -  TestTask 2 (6c239ae8-138c-4f04-9b02-427d7d33865c)
22.01.2018 22:44:14 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.5 )'
22.01.2018 22:44:14 -  with parameters: 
22.01.2018 22:44:14 - etf.testcases = *
22.01.2018 22:44:14 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
22.01.2018 22:44:14 - Setting state to CREATED
22.01.2018 22:44:14 - Changed state from CREATED to INITIALIZING
22.01.2018 22:44:14 - Starting TestRun.26e1d1b9-7ebe-49de-a8a7-44228a006b06 at 2018-01-22T22:44:16+01:00
22.01.2018 22:44:16 - Changed state from INITIALIZING to INITIALIZED
22.01.2018 22:44:16 - TestRunTask initialized
22.01.2018 22:44:16 - Creating new tests databases to speed up tests.
22.01.2018 22:44:16 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
22.01.2018 22:44:16 - Optimizing last database etf-tdb-8e839e3c-f2ca-43e9-ba2f-f871364d92e0-0 
22.01.2018 22:44:16 - Import completed
22.01.2018 22:44:23 - Validation ended with 0 error(s)
22.01.2018 22:44:23 - Compiling test script
22.01.2018 22:44:23 - Starting XQuery tests
22.01.2018 22:44:23 - "Testing 1 records"
22.01.2018 22:44:23 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
22.01.2018 22:44:23 - "Statistics table: 0 ms"
22.01.2018 22:44:23 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
22.01.2018 22:44:23 - "Test Case 'Schema validation' started"
22.01.2018 22:44:26 - "Validating file GetRecordByIdResponse.xml: 3135 ms"
22.01.2018 22:44:26 - "Test Assertion 'md-xml.a.1: Validate XML documents': PASSED - 3135 ms"
22.01.2018 22:44:26 - "Test Case 'Schema validation' finished: PASSED"
22.01.2018 22:44:26 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: PASSED"
22.01.2018 22:44:26 - Releasing resources
22.01.2018 22:44:26 - TestRunTask initialized
22.01.2018 22:44:26 - Recreating new tests databases as the Test Object has changed!
22.01.2018 22:44:26 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
22.01.2018 22:44:26 - Optimizing last database etf-tdb-8e839e3c-f2ca-43e9-ba2f-f871364d92e0-0 
22.01.2018 22:44:26 - Import completed
22.01.2018 22:44:27 - Validation ended with 0 error(s)
22.01.2018 22:44:27 - Compiling test script
22.01.2018 22:44:27 - Starting XQuery tests
22.01.2018 22:44:27 - "Testing 1 records"
22.01.2018 22:44:27 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
22.01.2018 22:44:27 - "Statistics table: 1 ms"
22.01.2018 22:44:27 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
22.01.2018 22:44:27 - "Test Case 'Common tests' started"
22.01.2018 22:44:27 - "Test Assertion 'md-iso.a.1: Title': PASSED - 0 ms"
22.01.2018 22:44:27 - "Test Assertion 'md-iso.a.2: Abstract': PASSED - 0 ms"
22.01.2018 22:44:27 - "Test Assertion 'md-iso.a.3: Access and use conditions': PASSED_MANUAL - 0 ms"
22.01.2018 22:44:27 - "Test Assertion 'md-iso.a.4: Public access': PASSED - 1 ms"
22.01.2018 22:44:27 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 0 ms"
22.01.2018 22:44:27 - "Test Assertion 'md-iso.a.6: Language': PASSED - 0 ms"
22.01.2018 22:44:27 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 0 ms"
22.01.2018 22:44:27 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
22.01.2018 22:44:27 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 0 ms"
22.01.2018 22:44:27 - "Test Assertion 'md-iso.a.10: Responsible party contact info': PASSED - 0 ms"
22.01.2018 22:44:27 - "Test Assertion 'md-iso.a.11: Responsible party role': PASSED - 0 ms"
22.01.2018 22:44:27 - "Test Case 'Common tests' finished: PASSED_MANUAL"
22.01.2018 22:44:27 - "Test Case 'Hierarchy level' started"
22.01.2018 22:44:27 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 0 ms"
22.01.2018 22:44:27 - "Test Case 'Hierarchy level' finished: PASSED"
22.01.2018 22:44:27 - "Test Case 'Dataset (series) tests' started"
22.01.2018 22:44:27 - "Test Assertion 'md-iso.c.1: Dataset identification': PASSED - 0 ms"
22.01.2018 22:44:27 - "Test Assertion 'md-iso.c.2: Dataset language': PASSED - 0 ms"
22.01.2018 22:44:27 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_transport_networks_wms/service.svc/get?'"
22.01.2018 22:44:27 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_transport_networks_wfs/service.svc/get?'"
22.01.2018 22:44:28 - "Test Assertion 'md-iso.c.3: Dataset linkage': PASSED_MANUAL - 894 ms"
22.01.2018 22:44:28 - "Test Assertion 'md-iso.c.4: Dataset conformity': PASSED - 0 ms"
22.01.2018 22:44:28 - "Test Assertion 'md-iso.c.5: Dataset topic': PASSED - 1 ms"
22.01.2018 22:44:28 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': PASSED_MANUAL - 0 ms"
22.01.2018 22:44:28 - "Test Assertion 'md-iso.c.7: Dataset lineage': PASSED - 0 ms"
22.01.2018 22:44:28 - "Test Case 'Dataset (series) tests' finished: PASSED_MANUAL"
22.01.2018 22:44:28 - "Test Case 'Service tests' started"
22.01.2018 22:44:28 - "Test Assertion 'md-iso.d.1: Service type': PASSED - 0 ms"
22.01.2018 22:44:28 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED - 0 ms"
22.01.2018 22:44:28 - "Test Assertion 'md-iso.d.3: Coupled resource': PASSED - 0 ms"
22.01.2018 22:44:28 - "Test Case 'Service tests' finished: PASSED"
22.01.2018 22:44:28 - "Test Case 'Keywords' started"
22.01.2018 22:44:28 - "Test Assertion 'md-iso.e.1: Keywords': PASSED - 1 ms"
22.01.2018 22:44:28 - "Test Case 'Keywords' finished: PASSED"
22.01.2018 22:44:28 - "Test Case 'Keywords - details' started"
22.01.2018 22:44:28 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.bg.atom'"
22.01.2018 22:44:29 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.cs.atom'"
22.01.2018 22:44:29 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.da.atom'"
22.01.2018 22:44:29 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.de.atom'"
22.01.2018 22:44:29 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.et.atom'"
22.01.2018 22:44:29 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.el.atom'"
22.01.2018 22:44:29 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.en.atom'"
22.01.2018 22:44:29 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.es.atom'"
22.01.2018 22:44:29 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.fr.atom'"
22.01.2018 22:44:30 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.hr.atom'"
22.01.2018 22:44:30 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.it.atom'"
22.01.2018 22:44:30 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.lv.atom'"
22.01.2018 22:44:30 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.lt.atom'"
22.01.2018 22:44:30 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.hu.atom'"
22.01.2018 22:44:30 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.mt.atom'"
22.01.2018 22:44:30 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.nl.atom'"
22.01.2018 22:44:30 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.pl.atom'"
22.01.2018 22:44:31 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.pt.atom'"
22.01.2018 22:44:31 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.ro.atom'"
22.01.2018 22:44:31 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.sk.atom'"
22.01.2018 22:44:31 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.sl.atom'"
22.01.2018 22:44:31 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.fi.atom'"
22.01.2018 22:44:31 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.sv.atom'"
22.01.2018 22:44:31 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.en.atom'"
22.01.2018 22:44:31 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 3674 ms"
22.01.2018 22:44:31 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 0 ms"
22.01.2018 22:44:31 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 0 ms"
22.01.2018 22:44:31 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 0 ms"
22.01.2018 22:44:31 - "Test Case 'Keywords - details' finished: PASSED"
22.01.2018 22:44:31 - "Test Case 'Temporal extent' started"
22.01.2018 22:44:31 - "Test Assertion 'md-iso.g.1: Temporal extent': PASSED - 0 ms"
22.01.2018 22:44:31 - "Test Case 'Temporal extent' finished: PASSED"
22.01.2018 22:44:31 - "Test Case 'Temporal extent - details' started"
22.01.2018 22:44:31 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 1 ms"
22.01.2018 22:44:31 - "Test Case 'Temporal extent - details' finished: PASSED"
22.01.2018 22:44:31 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: PASSED_MANUAL"
22.01.2018 22:44:32 - Releasing resources
22.01.2018 22:44:32 - Changed state from INITIALIZED to RUNNING
22.01.2018 22:44:32 - Duration: 18sec
22.01.2018 22:44:32 - TestRun finished
22.01.2018 22:44:32 - Changed state from RUNNING to COMPLETED
