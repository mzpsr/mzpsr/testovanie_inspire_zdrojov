06.02.2018 18:31:08 - Preparing Test Run interoperability_17316219_Geodetick&yacute;akartografick&yacute;&uacute;stavBratislava_tn-ro:FormOfWay (initiated Tue Feb 06 18:31:08 CET 2018)
06.02.2018 18:31:08 - Resolving Executable Test Suite dependencies
06.02.2018 18:31:08 - Preparing 11 Test Task:
06.02.2018 18:31:08 -  TestTask 1 (9fc15b84-32cd-4d2d-bd89-0fe50586c82b)
06.02.2018 18:31:08 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML encoding (EID: 545f9e49-009b-4114-9333-7ca26413b5d4, V: 0.2.1 )'
06.02.2018 18:31:08 -  with parameters: 
06.02.2018 18:31:08 - etf.testcases = *
06.02.2018 18:31:08 -  TestTask 2 (ea1de823-dbaf-4c36-a04a-f2465ae3481e)
06.02.2018 18:31:08 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements (EID: 09820daf-62b2-4fa3-a95f-56a0d2b7c4d8, V: 0.2.4 )'
06.02.2018 18:31:08 -  with parameters: 
06.02.2018 18:31:08 - etf.testcases = *
06.02.2018 18:31:08 -  TestTask 3 (ab0987c2-e18b-4990-929e-246ae0dc279d)
06.02.2018 18:31:08 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.9af1c865-1cf0-43ff-9250-069df01b0948'
06.02.2018 18:31:08 -  with parameters: 
06.02.2018 18:31:08 - etf.testcases = *
06.02.2018 18:31:08 -  TestTask 4 (27a6c87f-712b-4b1f-b926-5cfdd7df7896)
06.02.2018 18:31:08 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.4441cbde-371f-4899-90b3-145f4fd08ebc'
06.02.2018 18:31:08 -  with parameters: 
06.02.2018 18:31:08 - etf.testcases = *
06.02.2018 18:31:08 -  TestTask 5 (4c4258fa-556c-439e-8ec5-5492c77dd52f)
06.02.2018 18:31:08 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Application schema, Road Transport Networks (EID: 14986e54-74c4-43b0-979b-d0d3e5cd0e8c, V: 0.2.1 )'
06.02.2018 18:31:08 -  with parameters: 
06.02.2018 18:31:08 - etf.testcases = *
06.02.2018 18:31:08 -  TestTask 6 (6b474abf-47d6-427a-ad0b-c12b7739fed7)
06.02.2018 18:31:08 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
06.02.2018 18:31:08 -  with parameters: 
06.02.2018 18:31:08 - etf.testcases = *
06.02.2018 18:31:08 -  TestTask 7 (ef6e50e5-e887-426c-bc00-c6b249d8786b)
06.02.2018 18:31:08 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Data consistency, Transport Networks (EID: 733af9a0-312b-4f71-9aa2-977cd2134d23, V: 0.2.2 )'
06.02.2018 18:31:08 -  with parameters: 
06.02.2018 18:31:08 - etf.testcases = *
06.02.2018 18:31:08 -  TestTask 8 (832be81c-acbd-401a-944e-9a6c17d1dcba)
06.02.2018 18:31:08 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
06.02.2018 18:31:08 -  with parameters: 
06.02.2018 18:31:08 - etf.testcases = *
06.02.2018 18:31:08 -  TestTask 9 (e501f99d-6835-40e4-8873-1d0a17caef33)
06.02.2018 18:31:08 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Transport Networks (EID: df5db9a4-b15f-4193-a6ff-6e9951af46f5, V: 0.2.2 )'
06.02.2018 18:31:08 -  with parameters: 
06.02.2018 18:31:08 - etf.testcases = *
06.02.2018 18:31:08 -  TestTask 10 (61856883-9795-4eba-934b-b54ef172fc09)
06.02.2018 18:31:08 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
06.02.2018 18:31:08 -  with parameters: 
06.02.2018 18:31:08 - etf.testcases = *
06.02.2018 18:31:08 -  TestTask 11 (cb683aca-eb77-4c42-978a-d1580d948950)
06.02.2018 18:31:08 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Reference systems, Transport Networks (EID: 9d35024d-9dd7-43a9-afff-d5aea5f51595, V: 0.2.0 )'
06.02.2018 18:31:08 -  with parameters: 
06.02.2018 18:31:08 - etf.testcases = *
06.02.2018 18:31:08 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
06.02.2018 18:31:08 - Setting state to CREATED
06.02.2018 18:31:08 - Changed state from CREATED to INITIALIZING
06.02.2018 18:31:08 - Starting TestRun.ea2da396-a698-4106-a94f-b24e16ed3101 at 2018-02-06T18:31:10+01:00
06.02.2018 18:31:10 - Changed state from INITIALIZING to INITIALIZED
06.02.2018 18:31:10 - TestRunTask initialized
06.02.2018 18:31:10 - Creating new tests databases to speed up tests.
06.02.2018 18:31:10 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
06.02.2018 18:31:10 - Optimizing last database etf-tdb-590752fd-bbd3-4d33-a076-972449c7ec7c-0 
06.02.2018 18:31:10 - Import completed
06.02.2018 18:31:13 - Validation ended with 0 error(s)
06.02.2018 18:31:13 - Compiling test script
06.02.2018 18:31:13 - Starting XQuery tests
06.02.2018 18:31:13 - "Testing 1 features"
06.02.2018 18:31:13 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-encoding/inspire-gml/ets-inspire-gml-bsxets.xml"
06.02.2018 18:31:13 - "Statistics table: 0 ms"
06.02.2018 18:31:13 - "Test Suite 'Conformance class: INSPIRE GML encoding' started"
06.02.2018 18:31:13 - "Test Case 'Basic tests' started"
06.02.2018 18:31:13 - "Test Assertion 'gml.a.1: Errors loading the XML documents': PASSED - 0 ms"
06.02.2018 18:31:13 - "Test Assertion 'gml.a.2: Document root element': PASSED - 0 ms"
06.02.2018 18:31:13 - "Test Assertion 'gml.a.3: Character encoding': NOT_APPLICABLE"
06.02.2018 18:31:13 - "Test Case 'Basic tests' finished: PASSED"
06.02.2018 18:31:13 - "Test Suite 'Conformance class: INSPIRE GML encoding' finished: PASSED"
06.02.2018 18:31:14 - Releasing resources
06.02.2018 18:31:14 - TestRunTask initialized
06.02.2018 18:31:14 - Recreating new tests databases as the Test Object has changed!
06.02.2018 18:31:14 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
06.02.2018 18:31:14 - Optimizing last database etf-tdb-590752fd-bbd3-4d33-a076-972449c7ec7c-0 
06.02.2018 18:31:14 - Import completed
06.02.2018 18:31:14 - Validation ended with 0 error(s)
06.02.2018 18:31:14 - Compiling test script
06.02.2018 18:31:14 - Starting XQuery tests
06.02.2018 18:31:14 - "Testing 1 features"
06.02.2018 18:31:14 - "Indexing features (parsing errors: 0): 1 ms"
06.02.2018 18:31:14 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/schemas/ets-schemas-bsxets.xml"
06.02.2018 18:31:14 - "Statistics table: 0 ms"
06.02.2018 18:31:14 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' started"
06.02.2018 18:31:14 - "Test Case 'Schema' started"
06.02.2018 18:31:14 - "Test Assertion 'gmlas.a.1: Mapping of source data to INSPIRE': PASSED_MANUAL"
06.02.2018 18:31:14 - "Test Assertion 'gmlas.a.2: Modelling of additional spatial object types': PASSED_MANUAL"
06.02.2018 18:31:14 - "Test Case 'Schema' finished: PASSED_MANUAL"
06.02.2018 18:31:14 - "Test Case 'Schema validation' started"
06.02.2018 18:31:14 - "Test Assertion 'gmlas.b.1: xsi:schemaLocation attribute': PASSED - 0 ms"
06.02.2018 18:31:14 - "Validating get.xml"
06.02.2018 18:31:21 - "Duration: 6999 ms. Errors: 0."
06.02.2018 18:31:21 - "Test Assertion 'gmlas.b.2: validate XML documents': PASSED - 7000 ms"
06.02.2018 18:31:21 - "Test Case 'Schema validation' finished: PASSED"
06.02.2018 18:31:21 - "Test Case 'GML model' started"
06.02.2018 18:31:21 - "Test Assertion 'gmlas.c.1: Consistency with the GML model': PASSED - 0 ms"
06.02.2018 18:31:21 - "Test Assertion 'gmlas.c.2: nilReason attributes require xsi:nil=true': PASSED - 1 ms"
06.02.2018 18:31:21 - "Test Assertion 'gmlas.c.3: nilReason values': PASSED - 0 ms"
06.02.2018 18:31:21 - "Test Case 'GML model' finished: PASSED"
06.02.2018 18:31:21 - "Test Case 'Simple features' started"
06.02.2018 18:31:21 - "Test Assertion 'gmlas.d.1: No spatial topology objects': PASSED - 0 ms"
06.02.2018 18:31:21 - "Test Assertion 'gmlas.d.2: No non-linear interpolation': PASSED - 0 ms"
06.02.2018 18:31:21 - "Test Assertion 'gmlas.d.3: Surface geometry elements': PASSED - 0 ms"
06.02.2018 18:31:21 - "Test Assertion 'gmlas.d.4: No non-planar interpolation': PASSED - 0 ms"
06.02.2018 18:31:21 - "Test Assertion 'gmlas.d.5: Geometry elements': PASSED - 0 ms"
06.02.2018 18:31:21 - "Test Assertion 'gmlas.d.6: Point coordinates in gml:pos': PASSED - 0 ms"
06.02.2018 18:31:21 - "Test Assertion 'gmlas.d.7: Curve/Surface coordinates in gml:posList': PASSED - 0 ms"
06.02.2018 18:31:21 - "Test Assertion 'gmlas.d.8: No array property elements': PASSED - 0 ms"
06.02.2018 18:31:21 - "Test Assertion 'gmlas.d.9: 1, 2 or 3 coordinate dimensions': PASSED - 0 ms"
06.02.2018 18:31:21 - "Test Assertion 'gmlas.d.10: Validate geometries (1)': PASSED - 22 ms"
06.02.2018 18:31:21 - "Test Assertion 'gmlas.d.11: Validate geometries (2)': PASSED - 0 ms"
06.02.2018 18:31:21 - "Test Case 'Simple features' finished: PASSED"
06.02.2018 18:31:21 - "Test Case 'Code list values in basic data types' started"
06.02.2018 18:31:21 - "Test Assertion 'gmlas.e.1: GrammaticalNumber attributes': PASSED - 7 ms"
06.02.2018 18:31:21 - "Test Assertion 'gmlas.e.2: GrammaticalGender attributes': PASSED - 3 ms"
06.02.2018 18:31:21 - "Test Assertion 'gmlas.e.3: NameStatus attributes': PASSED - 3 ms"
06.02.2018 18:31:21 - "Test Assertion 'gmlas.e.4: Nativeness attributes': PASSED - 3 ms"
06.02.2018 18:31:21 - "Test Case 'Code list values in basic data types' finished: PASSED"
06.02.2018 18:31:21 - "Test Case 'Constraints' started"
06.02.2018 18:31:21 - "Test Assertion 'gmlas.f.1: At least one of the two attributes pronunciationSoundLink and pronunciationIPA shall not be void': PASSED - 0 ms"
06.02.2018 18:31:21 - "Test Case 'Constraints' finished: PASSED"
06.02.2018 18:31:21 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' finished: PASSED_MANUAL"
06.02.2018 18:31:22 - Releasing resources
06.02.2018 18:31:22 - TestRunTask initialized
06.02.2018 18:31:22 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
06.02.2018 18:31:22 - Validation ended with 0 error(s)
06.02.2018 18:31:22 - Compiling test script
06.02.2018 18:31:22 - Starting XQuery tests
06.02.2018 18:31:22 - "Testing 1 features"
06.02.2018 18:31:22 - "Indexing features (parsing errors: 0): 0 ms"
06.02.2018 18:31:22 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-gml/ets-tn-gml-bsxets.xml"
06.02.2018 18:31:22 - "Statistics table: 1 ms"
06.02.2018 18:31:22 - "Test Suite 'Conformance class: GML application schemas, Transport Networks' started"
06.02.2018 18:31:22 - "Test Case 'Basic test' started"
06.02.2018 18:31:22 - "Test Assertion 'tn-gml.a.1: Transport Network feature in dataset': PASSED - 0 ms"
06.02.2018 18:31:22 - "Test Case 'Basic test' finished: PASSED"
06.02.2018 18:31:22 - "Test Suite 'Conformance class: GML application schemas, Transport Networks' finished: PASSED"
06.02.2018 18:31:22 - Releasing resources
06.02.2018 18:31:22 - TestRunTask initialized
06.02.2018 18:31:22 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
06.02.2018 18:31:22 - Validation ended with 0 error(s)
06.02.2018 18:31:22 - Compiling test script
06.02.2018 18:31:22 - Starting XQuery tests
06.02.2018 18:31:22 - "Testing 1 features"
06.02.2018 18:31:22 - "Indexing features (parsing errors: 0): 0 ms"
06.02.2018 18:31:22 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-as/ets-tn-as-bsxets.xml"
06.02.2018 18:31:22 - "Statistics table: 0 ms"
06.02.2018 18:31:22 - "Test Suite 'Conformance class: Application schema, Transport Networks Common' started"
06.02.2018 18:31:22 - "Test Case 'Code list values' started"
06.02.2018 18:31:22 - "Test Assertion 'tn-as.a.1: AccessRestrictionValue attributes': PASSED - 5 ms"
06.02.2018 18:31:22 - "Test Assertion 'tn-as.a.2: RestrictionTypeValue attributes': PASSED - 5 ms"
06.02.2018 18:31:22 - "Test Case 'Code list values' finished: PASSED"
06.02.2018 18:31:22 - "Test Case 'Constraints' started"
06.02.2018 18:31:22 - "Test Assertion 'tn-as.b.1: TrafficFlowDirection can only be associated with a spatial object of the type Link or LinkSequence.': PASSED - 0 ms"
06.02.2018 18:31:22 - "Test Assertion 'tn-as.b.2: All transport areas have an external object identifier.': PASSED - 0 ms"
06.02.2018 18:31:22 - "Test Assertion 'tn-as.b.3: All transport links have an external object identifier.': PASSED - 0 ms"
06.02.2018 18:31:22 - "Test Assertion 'tn-as.b.4: All transport link sequences have an external object identifier.': PASSED - 0 ms"
06.02.2018 18:31:22 - "Test Assertion 'tn-as.b.5: All transport link sets have an external object identifier.': PASSED - 0 ms"
06.02.2018 18:31:22 - "Test Assertion 'tn-as.b.6: All transport nodes have an external object identifier.': PASSED - 0 ms"
06.02.2018 18:31:22 - "Test Assertion 'tn-as.b.7: All transport points have an external object identifier.': PASSED - 0 ms"
06.02.2018 18:31:22 - "Test Assertion 'tn-as.b.8: All transport properties have an external object identifier.': PASSED - 0 ms"
06.02.2018 18:31:22 - "Test Assertion 'tn-as.b.9: A transport link sequence must be composed of transport links that all belong to the same transport network.': PASSED - 0 ms"
06.02.2018 18:31:22 - "Test Assertion 'tn-as.b.10: A transport link set must be composed of transport links and or transport link sequences that all belong to the same transport network.': PASSED - 0 ms"
06.02.2018 18:31:22 - "Test Case 'Constraints' finished: PASSED"
06.02.2018 18:31:22 - "Test Case 'Geometry' started"
06.02.2018 18:31:22 - "Test Assertion 'tn-as.c.1: No free transport nodes': PASSED - 0 ms"
06.02.2018 18:31:22 - "Test Assertion 'tn-as.c.2: Intersections only at crossings': PASSED_MANUAL"
06.02.2018 18:31:22 - "Test Case 'Geometry' finished: PASSED_MANUAL"
06.02.2018 18:31:22 - "Test Case 'Network connectivity' started"
06.02.2018 18:31:22 - "Test Assertion 'tn-as.d.1: Connectivity at crossings': PASSED_MANUAL"
06.02.2018 18:31:22 - "Test Assertion 'tn-as.d.2: Unconnected nodes': PASSED_MANUAL"
06.02.2018 18:31:22 - "Test Assertion 'tn-as.d.3: Connectivity tolerance': PASSED_MANUAL"
06.02.2018 18:31:22 - "Test Case 'Network connectivity' finished: PASSED_MANUAL"
06.02.2018 18:31:22 - "Test Case 'Object References' started"
06.02.2018 18:31:22 - "Test Assertion 'tn-as.e.1: Linear references': PASSED_MANUAL"
06.02.2018 18:31:22 - "Test Assertion 'tn-as.e.2: Inter-modal connections': PASSED - 0 ms"
06.02.2018 18:31:22 - "Test Case 'Object References' finished: PASSED_MANUAL"
06.02.2018 18:31:22 - "Test Suite 'Conformance class: Application schema, Transport Networks Common' finished: PASSED_MANUAL"
06.02.2018 18:31:23 - Releasing resources
06.02.2018 18:31:23 - TestRunTask initialized
06.02.2018 18:31:23 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
06.02.2018 18:31:23 - Validation ended with 0 error(s)
06.02.2018 18:31:23 - Compiling test script
06.02.2018 18:31:23 - Starting XQuery tests
06.02.2018 18:31:23 - "Testing 1 features"
06.02.2018 18:31:23 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-ro-as/ets-tn-ro-as-bsxets.xml"
06.02.2018 18:31:23 - "Statistics table: 0 ms"
06.02.2018 18:31:23 - "Test Suite 'Conformance class: Application schema, Road Transport Networks' started"
06.02.2018 18:31:23 - "Test Case 'Code list values' started"
06.02.2018 18:31:23 - "Test Assertion 'tn-ro-as.a.1: AreaConditionValue attributes': PASSED - 7 ms"
06.02.2018 18:31:23 - "Test Assertion 'tn-ro-as.a.2: FormOfRoadNodeValue attributes': PASSED - 4 ms"
06.02.2018 18:31:23 - "Test Assertion 'tn-ro-as.a.3: FormOfWayValue attributes': PASSED - 5 ms"
06.02.2018 18:31:23 - "Test Assertion 'tn-ro-as.a.4: RoadPartValue attributes': PASSED - 61 ms"
06.02.2018 18:31:23 - "Test Assertion 'tn-ro-as.a.5: RoadServiceTypeValue attributes': PASSED - 4 ms"
06.02.2018 18:31:23 - "Test Assertion 'tn-ro-as.a.6: RoadSurfaceCategoryValue attributes': PASSED - 24 ms"
06.02.2018 18:31:23 - "Test Assertion 'tn-ro-as.a.7: ServiceFacilityValue attributes': PASSED - 4 ms"
06.02.2018 18:31:23 - "Test Assertion 'tn-ro-as.a.8: SpeedLimitSourceValue attributes': PASSED - 18 ms"
06.02.2018 18:31:23 - "Test Assertion 'tn-ro-as.a.9: VehicleTypeValue attributes': PASSED - 30 ms"
06.02.2018 18:31:23 - "Test Assertion 'tn-ro-as.a.10: WeatherConditionValue attributes': PASSED - 4 ms"
06.02.2018 18:31:23 - "Test Case 'Code list values' finished: PASSED"
06.02.2018 18:31:23 - "Test Case 'Constraints' started"
06.02.2018 18:31:23 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_transport_networks_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=netElementL.1400000'"
06.02.2018 18:31:53 - "Exception: Timeout exceeded. URL: https://zbgisws.skgeodesy.sk/inspire_transport_networks_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=netElementL.1400000"
06.02.2018 18:31:53 - "Test Assertion 'tn-ro-as.b.1: FormOfWay can only be associated with a spatial object that is part of a road transport network.': FAILED - 30008 ms"
06.02.2018 18:31:53 - "Test Assertion 'tn-ro-as.b.2: FunctionalRoadClass can only be associated with a spatial object that is part of a road transport network.': PASSED - 0 ms"
06.02.2018 18:31:53 - "Test Assertion 'tn-ro-as.b.3: NumberOfLanes can only be associated with a spatial object that is part of a road transport network.': PASSED - 0 ms"
06.02.2018 18:31:53 - "Test Assertion 'tn-ro-as.b.4: RoadName can only be associated with a spatial object that is part of a road transport network.': PASSED - 0 ms"
06.02.2018 18:31:53 - "Test Assertion 'tn-ro-as.b.5: RoadSurfaceCategory can only be associated with a spatial object that is part of a road transport network.': PASSED - 0 ms"
06.02.2018 18:31:53 - "Test Assertion 'tn-ro-as.b.6: RoadWidth can only be associated with a spatial object that is part of a road transport network.': PASSED - 0 ms"
06.02.2018 18:31:53 - "Test Assertion 'tn-ro-as.b.7: SpeedLimit can only be associated with a spatial object that is part of a road transport network.': PASSED - 0 ms"
06.02.2018 18:31:53 - "Test Assertion 'tn-ro-as.b.8: RoadServiceType can only be associated with a spatial object of the type RoadServiceArea or RoadNode (when formOfRoadNode=roadServiceArea)': PASSED - 0 ms"
06.02.2018 18:31:53 - "Test Case 'Constraints' finished: FAILED"
06.02.2018 18:31:53 - "Test Case 'Link centrelines' started"
06.02.2018 18:31:53 - "Test Assertion 'tn-ro-as.c.1: Link centrelines test': PASSED_MANUAL"
06.02.2018 18:31:53 - "Test Case 'Link centrelines' finished: PASSED_MANUAL"
06.02.2018 18:31:53 - "Test Suite 'Conformance class: Application schema, Road Transport Networks' finished: FAILED"
06.02.2018 18:31:54 - Releasing resources
06.02.2018 18:31:54 - TestRunTask initialized
06.02.2018 18:31:54 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
06.02.2018 18:31:54 - Validation ended with 0 error(s)
06.02.2018 18:31:54 - Compiling test script
06.02.2018 18:31:54 - Starting XQuery tests
06.02.2018 18:31:54 - "Testing 1 features"
06.02.2018 18:31:54 - "Indexing features (parsing errors: 0): 0 ms"
06.02.2018 18:31:54 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/data-consistency/ets-data-consistency-bsxets.xml"
06.02.2018 18:31:54 - "Statistics table: 0 ms"
06.02.2018 18:31:54 - "Test Suite 'Conformance class: Data consistency, General requirements' started"
06.02.2018 18:31:54 - "Test Case 'Version consistency' started"
06.02.2018 18:31:54 - "Test Assertion 'dc.a.1: Version lifespan plausible': PASSED - 0 ms"
06.02.2018 18:31:54 - "Test Assertion 'dc.a.2: Unique identifier persistency': PASSED_MANUAL"
06.02.2018 18:31:54 - "Test Assertion 'dc.a.3: Spatial object type stable': PASSED_MANUAL"
06.02.2018 18:31:54 - "Test Case 'Version consistency' finished: PASSED_MANUAL"
06.02.2018 18:31:54 - "Test Case 'Temporal consistency' started"
06.02.2018 18:31:54 - "Test Assertion 'dc.b.1: Valid time plausible': PASSED - 0 ms"
06.02.2018 18:31:54 - "Test Case 'Temporal consistency' finished: PASSED"
06.02.2018 18:31:54 - "Test Suite 'Conformance class: Data consistency, General requirements' finished: PASSED_MANUAL"
06.02.2018 18:31:54 - Releasing resources
06.02.2018 18:31:54 - TestRunTask initialized
06.02.2018 18:31:54 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
06.02.2018 18:31:54 - Validation ended with 0 error(s)
06.02.2018 18:31:54 - Compiling test script
06.02.2018 18:31:54 - Starting XQuery tests
06.02.2018 18:31:54 - "Testing 1 features"
06.02.2018 18:31:54 - "Indexing features (parsing errors: 0): 0 ms"
06.02.2018 18:31:54 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-dc/ets-tn-dc-bsxets.xml"
06.02.2018 18:31:54 - "Statistics table: 0 ms"
06.02.2018 18:31:54 - "Test Suite 'Conformance class: Data consistency, Transport Networks' started"
06.02.2018 18:31:54 - "Test Case 'Spatial consistency' started"
06.02.2018 18:31:54 - "Test Assertion 'tn-dc.a.1: Each Transport Network Link or Node geometry is within a Network Area geometry (Road Transport Networks)': PASSED - 0 ms"
06.02.2018 18:31:54 - "Test Assertion 'tn-dc.a.2: Each Transport Network Link or Node geometry is within a Network Area geometry (Railway Transport Networks)': PASSED - 0 ms"
06.02.2018 18:31:54 - "Test Assertion 'tn-dc.a.3: Each Transport Network Link or Node geometry is within a Network Area geometry (Waterway Transport Networks)': PASSED - 0 ms"
06.02.2018 18:31:54 - "Test Assertion 'tn-dc.a.4: Each Transport Network Link or Node geometry is within a Network Area geometry (Air Transport Networks)': PASSED - 0 ms"
06.02.2018 18:31:54 - "Test Assertion 'tn-dc.a.5: Manual review': PASSED_MANUAL"
06.02.2018 18:31:54 - "Test Case 'Spatial consistency' finished: PASSED_MANUAL"
06.02.2018 18:31:54 - "Test Suite 'Conformance class: Data consistency, Transport Networks' finished: PASSED_MANUAL"
06.02.2018 18:31:55 - Releasing resources
06.02.2018 18:31:55 - TestRunTask initialized
06.02.2018 18:31:55 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
06.02.2018 18:31:55 - Validation ended with 0 error(s)
06.02.2018 18:31:55 - Compiling test script
06.02.2018 18:31:55 - Starting XQuery tests
06.02.2018 18:31:55 - "Testing 1 features"
06.02.2018 18:31:55 - "Indexing features (parsing errors: 0): 0 ms"
06.02.2018 18:31:55 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/information-accessibility/ets-information-accessibility-bsxets.xml"
06.02.2018 18:31:55 - "Statistics table: 0 ms"
06.02.2018 18:31:55 - "Test Suite 'Conformance class: Information accessibility, General requirements' started"
06.02.2018 18:31:55 - "Test Case 'Coordinate reference systems (CRS)' started"
06.02.2018 18:31:55 - "Test Assertion 'ia.a.1: CRS publicly accessible via HTTP': PASSED - 0 ms"
06.02.2018 18:31:55 - "Test Case 'Coordinate reference systems (CRS)' finished: PASSED"
06.02.2018 18:31:55 - "Test Suite 'Conformance class: Information accessibility, General requirements' finished: PASSED"
06.02.2018 18:31:55 - Releasing resources
06.02.2018 18:31:55 - TestRunTask initialized
06.02.2018 18:31:55 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
06.02.2018 18:31:55 - Validation ended with 0 error(s)
06.02.2018 18:31:55 - Compiling test script
06.02.2018 18:31:55 - Starting XQuery tests
06.02.2018 18:31:55 - "Testing 1 features"
06.02.2018 18:31:55 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-ia/ets-tn-ia-bsxets.xml"
06.02.2018 18:31:55 - "Statistics table: 0 ms"
06.02.2018 18:31:55 - "Test Suite 'Conformance class: Information accessibility, Transport Networks' started"
06.02.2018 18:31:55 - "Test Case 'Code lists' started"
06.02.2018 18:31:55 - "Test Assertion 'tn-ia.a.1: Code list extensions accessible': PASSED - 0 ms"
06.02.2018 18:31:55 - "Test Case 'Code lists' finished: PASSED"
06.02.2018 18:31:55 - "Test Case 'Feature references' started"
06.02.2018 18:31:55 - "Test Assertion 'tn-ia.b.1: MarkerPost.route': PASSED - 0 ms"
06.02.2018 18:31:55 - "Test Assertion 'tn-ia.b.2: TransportLinkSet.post': PASSED - 0 ms"
06.02.2018 18:31:55 - "Test Case 'Feature references' finished: PASSED"
06.02.2018 18:31:55 - "Test Suite 'Conformance class: Information accessibility, Transport Networks' finished: PASSED"
06.02.2018 18:31:56 - Releasing resources
06.02.2018 18:31:56 - TestRunTask initialized
06.02.2018 18:31:56 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
06.02.2018 18:31:56 - Validation ended with 0 error(s)
06.02.2018 18:31:56 - Compiling test script
06.02.2018 18:31:56 - Starting XQuery tests
06.02.2018 18:31:56 - "Testing 1 features"
06.02.2018 18:31:56 - "Indexing features (parsing errors: 0): 1 ms"
06.02.2018 18:31:56 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/reference-systems/ets-reference-systems-bsxets.xml"
06.02.2018 18:31:56 - "Statistics table: 0 ms"
06.02.2018 18:31:56 - "Test Suite 'Conformance class: Reference systems, General requirements' started"
06.02.2018 18:31:56 - "Test Case 'Spatial reference systems' started"
06.02.2018 18:31:56 - "Test Assertion 'rs.a.1: Spatial reference systems in feature geometries': PASSED - 0 ms"
06.02.2018 18:31:56 - "Test Assertion 'rs.a.2: Default spatial reference systems in feature collections': PASSED - 0 ms"
06.02.2018 18:31:56 - "Test Case 'Spatial reference systems' finished: PASSED"
06.02.2018 18:31:56 - "Test Case 'Temporal reference systems' started"
06.02.2018 18:31:56 - "Test Assertion 'rs.a.3: Temporal reference systems in features': PASSED - 0 ms"
06.02.2018 18:31:56 - "Test Case 'Temporal reference systems' finished: PASSED"
06.02.2018 18:31:56 - "Test Suite 'Conformance class: Reference systems, General requirements' finished: PASSED"
06.02.2018 18:31:56 - Releasing resources
06.02.2018 18:31:56 - TestRunTask initialized
06.02.2018 18:31:56 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
06.02.2018 18:31:56 - Validation ended with 0 error(s)
06.02.2018 18:31:56 - Compiling test script
06.02.2018 18:31:56 - Starting XQuery tests
06.02.2018 18:31:56 - "Testing 1 features"
06.02.2018 18:31:56 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-tn/tn-rs/ets-tn-rs-bsxets.xml"
06.02.2018 18:31:56 - "Statistics table: 0 ms"
06.02.2018 18:31:56 - "Test Suite 'Conformance class: Reference systems, Transport Networks' started"
06.02.2018 18:31:56 - "Test Case 'Additional theme-specific rules for reference systems' started"
06.02.2018 18:31:56 - "Test Assertion 'tn-rs.a.1: Test always passes': PASSED - 0 ms"
06.02.2018 18:31:56 - "Test Case 'Additional theme-specific rules for reference systems' finished: PASSED"
06.02.2018 18:31:56 - "Test Suite 'Conformance class: Reference systems, Transport Networks' finished: PASSED"
06.02.2018 18:31:56 - Releasing resources
06.02.2018 18:31:56 - Changed state from INITIALIZED to RUNNING
06.02.2018 18:31:57 - Duration: 48sec
06.02.2018 18:31:57 - TestRun finished
06.02.2018 18:31:57 - Changed state from RUNNING to COMPLETED
