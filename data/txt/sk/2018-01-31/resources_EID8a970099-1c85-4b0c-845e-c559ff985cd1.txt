31.01.2018 01:03:00 - Preparing Test Run sk_testing_inspire_resources_app__undefined__RUN__EIDed2d3501-d700-4ff9-b9bf-070dece8ddbd__2 (initiated Wed Jan 31 01:03:00 CET 2018)
31.01.2018 01:03:00 - Resolving Executable Test Suite dependencies
31.01.2018 01:03:00 - Preparing 2 Test Task:
31.01.2018 01:03:00 -  TestTask 1 (c02912e8-4bf1-4e06-b009-104f11fd2334)
31.01.2018 01:03:00 -  will perform tests on Test Object 'INSPIRE Download Service - Cadastral Parcel - UO' by using Executable Test Suite 'WFS 2.0 (OGC 09-025r2/ISO 19142) Conformance Test Suite (EID: 95a1b6fc-2b55-3d43-9502-3b8b605bda10, V: 1.26.0 )'
31.01.2018 01:03:00 -  with parameters: 
31.01.2018 01:03:00 - etf.testcases = *
31.01.2018 01:03:00 -  TestTask 2 (791f82d9-144d-427e-843b-c727f6991acc)
31.01.2018 01:03:00 -  will perform tests on Test Object 'INSPIRE Download Service - Cadastral Parcel - UO' by using Executable Test Suite 'Conformance Class: Download Service - Direct WFS (EID: ed2d3501-d700-4ff9-b9bf-070dece8ddbd, V: 1.0.2 )'
31.01.2018 01:03:00 -  with parameters: 
31.01.2018 01:03:00 - etf.testcases = *
31.01.2018 01:03:00 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
31.01.2018 01:03:00 - Setting state to CREATED
31.01.2018 01:03:00 - Changed state from CREATED to INITIALIZING
31.01.2018 01:03:00 - Starting TestRun.8a970099-1c85-4b0c-845e-c559ff985cd1 at 2018-01-31T01:03:01+01:00
31.01.2018 01:03:01 - Changed state from INITIALIZING to INITIALIZED
31.01.2018 01:03:01 - TestRunTask initialized
31.01.2018 01:03:01 - Invoking TEAM Engine remotely. This may take a while. Progress messages are not supported.
31.01.2018 01:03:01 - Timeout is set to: 20min
31.01.2018 01:05:58 - Results received.
31.01.2018 01:05:59 - Internal ETS model updated.
31.01.2018 01:05:59 - Transforming results.
31.01.2018 01:05:59 - 50 of 5068 assertions passed
31.01.2018 01:06:00 - Releasing resources
31.01.2018 01:06:00 - Project Properties: 
31.01.2018 01:06:00 - etf.testcases - * 
31.01.2018 01:06:00 - serviceEndpoint - https://inspire.skgeodesy.sk/eskn/rest/services/INSPIREWFS/uo_wfs_inspire/GeoDataServer/exts/InspireFeatureDownload/service?ACCEPTVERSIONS=2.0.0&request=GetCapabilities&service=WFS&VERSION=2.0.0 
31.01.2018 01:06:00 - username -  
31.01.2018 01:06:00 - authUser -  
31.01.2018 01:06:00 - authMethod - basic 
31.01.2018 01:06:00 - TestRunTask initialized
31.01.2018 01:06:08 - Releasing resources
31.01.2018 01:06:08 - Changed state from INITIALIZED to RUNNING
31.01.2018 01:06:08 - Duration: 3min
31.01.2018 01:06:08 - TestRun finished
31.01.2018 01:06:08 - Changed state from RUNNING to COMPLETED
