31.01.2018 00:19:47 - Preparing Test Run sk_testing_inspire_resources_app__undefined__RUN__EIDed2d3501-d700-4ff9-b9bf-070dece8ddbd__2 (initiated Wed Jan 31 00:19:47 CET 2018)
31.01.2018 00:19:47 - Resolving Executable Test Suite dependencies
31.01.2018 00:19:47 - Preparing 2 Test Task:
31.01.2018 00:19:47 -  TestTask 1 (91eb7e28-9aef-4970-8e5d-4ee4fde2b2c3)
31.01.2018 00:19:47 -  will perform tests on Test Object 'INSPIRE Download Service - Cadastral Parcel - UO' by using Executable Test Suite 'WFS 2.0 (OGC 09-025r2/ISO 19142) Conformance Test Suite (EID: 95a1b6fc-2b55-3d43-9502-3b8b605bda10, V: 1.26.0 )'
31.01.2018 00:19:47 -  with parameters: 
31.01.2018 00:19:47 - etf.testcases = *
31.01.2018 00:19:47 -  TestTask 2 (07d9f889-2971-430f-87da-0144c21f1101)
31.01.2018 00:19:47 -  will perform tests on Test Object 'INSPIRE Download Service - Cadastral Parcel - UO' by using Executable Test Suite 'Conformance Class: Download Service - Direct WFS (EID: ed2d3501-d700-4ff9-b9bf-070dece8ddbd, V: 1.0.2 )'
31.01.2018 00:19:47 -  with parameters: 
31.01.2018 00:19:47 - etf.testcases = *
31.01.2018 00:19:47 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
31.01.2018 00:19:47 - Setting state to CREATED
31.01.2018 00:19:47 - Changed state from CREATED to INITIALIZING
31.01.2018 00:19:47 - Starting TestRun.6ebc1c7d-aa0e-426a-a6f2-c58052d0c2cd at 2018-01-31T00:19:48+01:00
31.01.2018 00:19:48 - Changed state from INITIALIZING to INITIALIZED
31.01.2018 00:19:48 - TestRunTask initialized
31.01.2018 00:19:48 - Invoking TEAM Engine remotely. This may take a while. Progress messages are not supported.
31.01.2018 00:19:48 - Timeout is set to: 20min
31.01.2018 00:38:22 - Results received.
31.01.2018 00:38:23 - Internal ETS model updated.
31.01.2018 00:38:23 - Transforming results.
31.01.2018 00:38:23 - 50 of 5068 assertions passed
31.01.2018 00:38:24 - Releasing resources
31.01.2018 00:38:24 - Project Properties: 
31.01.2018 00:38:24 - etf.testcases - * 
31.01.2018 00:38:24 - serviceEndpoint - https://inspire.skgeodesy.sk/eskn/rest/services/INSPIREWFS/uo_wfs_inspire/GeoDataServer/exts/InspireFeatureDownload/service?ACCEPTVERSIONS=2.0.0&request=GetCapabilities&service=WFS&VERSION=2.0.0 
31.01.2018 00:38:24 - username -  
31.01.2018 00:38:24 - authUser -  
31.01.2018 00:38:24 - authMethod - basic 
31.01.2018 00:38:24 - TestRunTask initialized
31.01.2018 00:38:32 - Releasing resources
31.01.2018 00:38:32 - Changed state from INITIALIZED to RUNNING
31.01.2018 00:38:32 - Duration: 18min
31.01.2018 00:38:32 - TestRun finished
31.01.2018 00:38:32 - Changed state from RUNNING to COMPLETED
