<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
$appDIR = "/var/www/klimeto/projects/2017/mzp/apps/testovanie_inspire_zdrojov/"; 
$appURL = "http://klimeto.com/projects/2017/mzp/apps/testovanie_inspire_zdrojov/";
$data = file_get_contents("php://input");
//var_dump($data);
//$data="https://rpi.gov.sk/rpi_csw/service.svc/get?request=GetRecordById&service=CSW&version=2.0.2&elementSetName=full&outputschema=http://www.isotc211.org/2005/gmd&Id=https://data.gov.sk/set/rpi/gmd/17316219/SK_UGKK_INSPIRE_WFS_HY";
$jsonData = json_decode($data);
//var_dump($jsonData);
$mdURL = $jsonData->testObject->resources->data;
$testLabel = $jsonData->label;
$etsID = $jsonData->executableTestSuiteIds[0];
$today = date('Y-m-d');
#var_dump($testLabel);
$testedResource = explode("_",$testLabel)[0];
$icoFolder = explode("_",$testLabel)[1];
$orgName = explode("_",$testLabel)[2];
$resTitle = explode("_",$testLabel)[3];
$resTitle = str_replace(' ','_',$resTitle);
$resTitle = str_replace(':','_',$resTitle);

#var_dump($testedResource);
#var_dump($icoFolder);
#var_dump($resTitle);
#exit();
$xmlFileContents = file_get_contents($mdURL);
if (strpos($xmlFileContents, '<csw:GetRecordByIdResponse') !== false) {
	$data = '';
    $cut1 = explode("<gmd:fileIdentifier>",$xmlFileContents)[1];
	$cut2 = str_replace("</gmd:MD_Metadata>","",str_replace("</csw:GetRecordByIdResponse>","",$cut1));
	$metadataString = "<gmd:fileIdentifier>" . $cut2;
	$gmdXML = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gml="http://www.opengis.net/gml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.isotc211.org/2005/gmd http://inspire.ec.europa.eu/draft-schemas/inspire-md-schemas/apiso-inspire/apiso-inspire.xsd http://www.isotc211.org/2005/srv http://inspire.ec.europa.eu/draft-schemas/inspire-md-schemas/srv/1.0/srv.xsd">';
	$gmdXML .= "<gmd:fileIdentifier>";
	$gmdXML .= $cut2;
	$gmdXML .= '</gmd:MD_Metadata>';
	file_put_contents($appDIR . 'data/xml/GetRecordByIdResponse.xml', $gmdXML);
	$data .= '{"label": "'.$testLabel.'","executableTestSuiteIds": ["'.$etsID.'"],"arguments": {},"testObject": {"resources": {"data": "'.$appURL.'data/xml/GetRecordByIdResponse.xml"}}}';
}
//$icoFromFileIdentifier = explode("</gmd:fileIdentifier>",(explode("<gmd:fileIdentifier>",$xmlFileContents)[1]))[0];
//preg_match('([0-9]{8})', $icoFromFileIdentifier, $icoFolderArray);
//$icoFolder = $icoFolderArray[0];

//Simple XML seems a bit too picky so we massage the response a bit
/*
$repl = str_replace('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', '', trim($xmlFileContents));
$repl = str_replace("\n", '', $repl); // remove new lines
$repl = str_replace("\r", '', $repl); // remove carriage returns
$repl = str_replace("  ", '', $repl); // remove spaces
$repl = trim($repl);
$resType = explode("<gmd:hierarchyLevel><gmd:MD_ScopeCode", $repl)[1];
$resType = explode("</gmd:MD_ScopeCode>", $resType)[0];
$resType= explode(">",$resType)[1];
$resTitle = explode("<gmd:citation><gmd:CI_Citation><gmd:title><gco:CharacterString>",$repl)[1];
$resTitle = explode("</gco:CharacterString>",$resTitle)[0];
$resTitle = str_replace(' ','_', $resTitle);
var_dump($resType);
var_dump($resTitle);
exit();
*/
/*
$orgName = explode("<gmd:organisationName><gco:CharacterString>",$repl)[1];
$orgName = explode("</gco:CharacterString>",$orgName)[0];
*/
if ($icoFolder == '' || $icoFolder == 'null'){
	if ($orgName != ''){
		$icoFolder = $orgName;
	}
	else{
		$icoFolder = 'nemaICO';
	}
}
/*
else{
	$request = 'https://datahub.ekosystem.slovensko.digital/api/datahub/corporate_bodies/search?access_token=1f68281897b51761619e9e331e20aa4ba24122b10c181301ec02588f0707b26d12d2bb168ed90ac4&q=cin:' . $icoFolder;
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $request);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
	$result = curl_exec($ch);
	curl_close($ch);
	$jsonObj = json_decode($result,true);
	if ($jsonObj['message'] == 'Not Found'){
		$icoFolder = $icoFolder;
	}
	else{
		$corpName = $jsonObj['name'];
		$icoFolder = str_replace(" ","_",str_replace('"','',str_replace(',','',$corpName)));
	};
}
*/
//preg_match("/^([A-Z]+)/", $orgName, $matches);
$icoFolder = $icoFolder ."_".$orgName;
//print $icoFolder;
// exit();
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'http://inspire-sandbox.jrc.ec.europa.eu/etf-webapp/v2/TestRuns');
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($ch);
$obj = json_decode($result, true);

if ($obj['error']){
	$out['error']= $error;
}
else{
	$ETStestUUID = $obj['EtfItemCollection']['testRuns']['TestRun']['id'];
	$ETStestLabel = $obj['EtfItemCollection']['testRuns']['TestRun']['label'];
	$testSTATUS = $obj['EtfItemCollection']['testRuns']['TestRun']['status'];
	$out['id']= $ETStestUUID;
	$out['status']= $testSTATUS;
	$completed = 0;
	while ($completed < 1){
		$json = file_get_contents('http://inspire-sandbox.jrc.ec.europa.eu/etf-webapp/v2/TestRuns/'.$ETStestUUID.'/progress?pos=0');
		$jsonObj = json_decode($json,true);
		$log = $jsonObj['val'];
		if ($log == 100){
			if(!is_dir($appDIR . 'data/html/'.$icoFolder.'/'.$testedResource.'/'.$today)){
				mkdir($appDIR . 'data/html/'.$icoFolder.'/'.$testedResource.'/'.$today,0777,true);
			}
			if(!is_dir($appDIR . 'data/txt/'.$icoFolder.'/'.$testedResource.'/'.$today)){
				mkdir($appDIR . 'data/txt/'.$icoFolder.'/'.$testedResource.'/'.$today,0777,true);
			}
			$reportFileName = $resTitle . '_' . $ETStestUUID;
			file_put_contents($appDIR . 'data/html/'.$icoFolder.'/'.$testedResource.'/'.$today.'/' .$reportFileName. '.html', fopen("http://inspire-sandbox.jrc.ec.europa.eu/etf-webapp/v2/TestRuns/".$ETStestUUID.".html", 'r'));
			$out['html'] = $appURL . 'data/html/'.$icoFolder.'/'.$testedResource.'/'.$today.'/' .$reportFileName. '.html';
			file_put_contents($appDIR . 'data/txt/'.$icoFolder.'/'.$testedResource.'/'.$today.'/' .$reportFileName. '.txt', fopen("http://inspire-sandbox.jrc.ec.europa.eu/etf-webapp/v2/TestRuns/".$ETStestUUID."/log", 'r'));
			$out['txt'] = $appURL . 'data/txt/'.$icoFolder.'/'.$testedResource.'/'.$today.'/' .$reportFileName. '.txt';
			$completed++;
			// DELETE THE TEST DATA FROM ETF
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'http://inspire-sandbox.jrc.ec.europa.eu/etf-webapp/v2/TestRuns/'.$ETStestUUID);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
			$result = curl_exec($ch);
			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);
		}
		else{
			$completed = 0;
		}
	}
}

echo json_encode($out,JSON_UNESCAPED_SLASHES);
?>
